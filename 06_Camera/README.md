[[_TOC_]]

# Kamera Konfiguration und Anleitung

## Verbindung
Verbinde das Flachband der Kamera mit der dafür vorgesehenen Steckverbindung:
![Kamera-Verbindung](img/Cam-install.JPG)

## Dokumentation
- Dokumentation zu Kameras: [Raspi-Kamera Hardware](https://www.raspberrypi.com/documentation/accessories/camera.html)
- Dokumentation zu Fotos und Videos: [Raspi-Kamera Software](https://www.raspberrypi.com/documentation/computers/camera_software.html#getting-started)

## Konfiguration
1. Editiere das config.txt:\
    `sudo nano /boot/firmware/config.txt`
2. Am Schluss des Files, füge `start_x=1` und `gpu_mem=128` hinzu.

3. Speichere und Schliesse das File

4. Vergewissere dich, dass die library für `raspistill` installiert ist. \
    Falls nicht: `sudo apt install libraspberrypi-bin`

5. Versuche mittels `raspistill -o Bild.jpg` ein Bild zu machen. \
    ! Fehlermeldung: *failed to open vchiq instance*.
    VCHIQ steht für VideoCore Host Interface Queue und ist die Schnittstelle zwischen CPU und GPU.
    Ändere die Berechtigungen für die Schnittstelle: \
    `sudo chmod 777 /dev/vchiq`\
    Versuche erneut ein Bild zu machen

**Achtung:** Möglicherweise ist ein zweiter Reboot nötig, bzw. `chmod` Befehl ausführen vor `raspistill`. `dtoverlay=imx219` war nicht nötig in config.txt, um Bilder bzw. Videos zu erfassen.

6. Führe ein reboot durch und vergewissere dich, dass `/dev/video0` vorhanden ist.

7. Versuche mittels `raspivid -p 0,0,640,480 -t 0` einen Videostream zu machen.

8. Eine andere Möglichkeit einen Videostream zu starten ist mit `ffmpeg`: \
    Installiere zuerst die library: `sudo apt install ffmpeg` \
    und starte dann den stream: `ffplay /dev/video0`

    Wenn ein Fehler bezüglich Zugriffsberechtigung ausgegeben wird, führe `sudo chmod 777 /dev/video0` aus und versuche erneut, den stream zu starten: `ffplay /dev/video0`

## Zugriff auf Kamerabilder mit Python (Open CV)

Auch opencv greift auf das video0 device zu. 

Opencv installation für Python mit:
```
pip install opencv-python
```

Teste den Bildzugriff mit dem einfachen Beispielskript:
```
python3 take_image.py
```

## Troubleshooting
### mmal Fehler
Wenn der/die Fehler unten ausgegeben werden, kann es sein, dass das Bild trotzdem gemacht wurde: überprüfe den Speicherort für das zu machende Bild.
```
mmal: mmal_vc_shm_init: could not initialize vc shared memory service
mmal: mmal_vc_component_create: failed to initialise shm for 'vc.camera_info' (7:EIO)
mmal: mmal_component_create_core: could not create component 'vc.camera_info' (7)
mmal: Failed to create camera_info component
```

