#!/usr/bin/env python3
"""
Author: Samu Huser
Date: 23.08.2021
Description: Python script to install software required on the Raspberry Pi for HSLU Digital Engineering.

These params will be changed:
eee_identity="eee-0xxxx"
eee_password="xxxx"
eee_hostname=eee_identity".simple.eee.intern"
"""
import os
import subprocess
import sys
import pwd as user_check
import time
import shlex
import ctypes
import socket


# configure this network device
DEFAULT_NETWORK_DEVICE = "wlan0"
EEE_REGISTRATION_SITE = "eeeportal.hslu.ch"
EEE_HOSTNAME_TRAIL = ".simple.eee.intern"
RASPI_DEFAULT_HOSTNAME = "raspberrypi"
RASPI_WIFI_COUNTRY_CODE = "CH"

# constants
IS_FRESH_INSTALL = ( socket.gethostname() == RASPI_DEFAULT_HOSTNAME )


# counter for configuration steps
stepCounter = 1

# global or individual consent
CONSENT_ALWAYS = False
networkDevice = DEFAULT_NETWORK_DEVICE
oldHostName = RASPI_DEFAULT_HOSTNAME

# debugging
BASH_VERBOSE = True


#--------------------------------------------------------------------------------------------	
class AdminStateUnknownError(Exception):
    """Cannot determine whether the user is an admin."""
    pass


#--------------------------------------------------------------------------------------------	
def is_user_admin():
    # type: () -> bool
    """Return True if user has admin privileges.

    Raises:
        AdminStateUnknownError if user privileges cannot be determined.
    """
    try:
        return os.getuid() == 0
    except AttributeError:
        pass
    try:
        return ctypes.windll.shell32.IsUserAnAdmin() == 1
    except AttributeError:
        raise AdminStateUnknownError


#--------------------------------------------------------------------------------------------	
def print_sep_line() -> None:
	print("\n-----------------------------------------------------------------------------\n")


#--------------------------------------------------------------------------------------------	
def consent() -> bool:
	if CONSENT_ALWAYS:
		return True
	reply = input( "Continue [yes / no] ? " ).strip().lower()
	return ( reply == "yes" )


#--------------------------------------------------------------------------------------------	
def wait_key():
    ''' Wait for a key press on the console and return it. '''
    result = None
    if os.name == 'nt':
        import msvcrt
        result = msvcrt.getch()
    else:
        import termios
        fd = sys.stdin.fileno()

        oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)

        try:
            result = sys.stdin.read(1)
        except IOError:
            pass
        finally:
            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)

    return result


#--------------------------------------------------------------------------------------------	
def highlight( txt ):
	return ">>> "+txt+" <<<"


#--------------------------------------------------------------------------------------------	
def print_banner() -> None:
	"""
	use -> https://patorjk.com/software/taag/#p=display&f=Slant&t=RASPI%20CONFIG
	"""
	banner = """
       ____  ___   _____ ____  ____   __________  _   __________________
      / __ \/   | / ___// __ \/  _/  / ____/ __ \/ | / / ____/  _/ ____/
     / /_/ / /| | \__ \/ /_/ // /   / /   / / / /  |/ / /_   / // / __  
    / _, _/ ___ |___/ / ____// /   / /___/ /_/ / /|  / __/ _/ // /_/ /  
   /_/ |_/_/  |_/____/_/   /___/   \____/\____/_/ |_/_/   /___/\____/   

    HSLU Digital Engineering - Raspi Config Tool
	"""
	print( banner )
	print_sep_line()
	print()
	print( "Press any key to continue ..." )
	wait_key ()


#--------------------------------------------------------------------------------------------	
def bash_cmd(bash_str: str, check=True, shell=False) -> subprocess.CompletedProcess:
	try:
		if BASH_VERBOSE:
			print( "> {}".format( bash_str ) )
			process = subprocess.run(bash_str.split(" "), check=check, shell=shell)
			return process
	except subprocess.CalledProcessError as e:
		print(e)
		sys.exit(1)


#--------------------------------------------------------------------------------------------	
def run_command(command) -> tuple:
	process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
	output = ""
	while True:
		line = process.stdout.readline()
		if ( len( line ) == 0 ) and ( process.poll() == 0 ): #is not None:
			break
		if line:
			#print( line.strip() )
			output += line.decode("utf-8").strip() + "\n"
	rc = process.poll()
	return rc, output


#--------------------------------------------------------------------------------------------	
def do_mac_address(device) -> bool:
	global stepCounter
	MAC_KEYWORD = "ether"

	print_sep_line()
	print( "{}. Identify MAC-address of WiFi network device:\n".format( stepCounter ) )
	stepCounter += 1

	success = False
	rc, output = run_command( "ifconfig "+device )
	for line in output.split('\n'):
		if line.startswith( MAC_KEYWORD ):
			s = len( MAC_KEYWORD )
			macAddress = line[ (s+1):(s+6*3) ]
			print( "MAC-address of raspi's {} device is \n{}\n\nUse this MAC-address to register Raspi on:\n{}\n\nNote:\n{} is only reachable from within the HSLU network.\n".format( device, highlight( macAddress ), highlight( EEE_REGISTRATION_SITE  ), EEE_REGISTRATION_SITE ) )
			success = True

	if not success:
		print( "WARN: could not identify MAC-address of device {}.\n      Run >>>ifconfig<<< manually and check whether device is correct.".format( device ) )

	print ( "Press any key to continue ..." )
	wait_key ()
	return success

# Hint:
# If the MAC address of your device is already registered by someone else on HSLU EEE portal,
# you can change the MAC address uding the following commands:
# > sudo ifconfig <interface-name> down
# > sudo ifconfig <interface-name> hw ether <new-mac-address> 
# > sudo ifconfig <interface-name> up
#


#--------------------------------------------------------------------------------------------	
def get_hslu_eee_credentials() -> tuple:
	"""
	:return: tuple of hostname, password and identity for EEE network
	"""
	global stepCounter
	print_sep_line()
	print( "{}. Register raspi as your device on HSLU's EEE-network:\n".format( stepCounter ) )
	stepCounter += 1


	print( "Information required below is available on site:\n{}\n".format( highlight( EEE_REGISTRATION_SITE ) ) )
	while True:
		eee_hostname = input("EEE-hostname (eee-0xxxx{}): ".format( EEE_HOSTNAME_TRAIL ) )
		if eee_hostname.find( EEE_HOSTNAME_TRAIL ) < 0:
			print( "Please, copy the entire hostname, including the trailing '{}'".format( EEE_HOSTNAME_TRAIL ) )
		else:
			break
	
	eee_password = input("EEE-password (click on eye symbol to make it visible): ")

	# clean inputs to avoid common pitfalls: added spaces, or hostname instead of identity
	eee_hostname = eee_hostname.strip()
	eee_password = eee_password.strip()

	# extract EEE identity from EEE hostname
	ix = eee_hostname.find( EEE_HOSTNAME_TRAIL )
	eee_identity = eee_hostname[ 0:ix ]

	# print("Start setup for HSLU Digital Engineering")
	return eee_hostname, eee_password, eee_identity


#--------------------------------------------------------------------------------------------	
def do_vnc() -> None:
    print_sep_line()
    print("Activating VNC Server")
    bash_cmd("sudo raspi-config nonint do_vnc 0", check=True, shell=False)
    bash_cmd("sudo raspi-config nonint get_vnc", check=True, shell=False)
    print("VNC Server activated")
    bash_cmd("sudo raspi-config nonint do_resolution 1920 1080", check=True, shell=False)

#--------------------------------------------------------------------------------------------		
def do_camera() -> None:
	# https://raspberrypi.stackexchange.com/questions/14229/how-can-i-enable-the-camera-without-using-raspi-config
	# https://github.com/l10n-tw/rc_gui/blob/master/src/rc_gui.c#L50-L100
	print_sep_line()
	print("Enabling camera")
	bash_cmd("raspi-config nonint do_camera 1", check=True, shell=False)
	# %D - Integer input - 0 is in general success / yes / selected, 1 is failed / no / not selected

#--------------------------------------------------------------------------------------------	
def is_pattern_in_file( filename, pattern ) -> bool:

	with open( filename, "rt" ) as f:
		data = f.read()
		return ( pattern in data )

	return False

#--------------------------------------------------------------------------------------------
def append_to_file( filename, part ):

	with open( filename, "a") as f:
		f.write( "\n\n" )
		f.write( part )

#--------------------------------------------------------------------------------------------	
def replace_in_file( filename, old, new ):

	replacedPattern = False
	with open( filename, "rt" ) as f:
		data = f.read()
		if data.find( old ):
			data = data.replace( old, new )
			replacedPattern = True

	if replacedPattern:
		with open( filename, "wt" ) as f:
			f.write( data )

	return replacedPattern
	
#--------------------------------------------------------------------------------------------	
def do_wpa_fix() -> None:
	print_sep_line()
	print("Fixing WPA2 enterprise WLAN bug")

	#
	filename = "/lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant"
	if not is_pattern_in_file( filename, "HSLU_EEE_FIX" ):
		if replace_in_file( filename, \
							"-nl80211,wext", \
							"-Wext,nl80211" ):
			print( "[INFO] fixed WPA2 enterprise WLAN bug in file: {}".format( filename ) )
	else:
		print( "[INFO] WPA2 patch already applied to file: {}".format( filename ) )
		
	# ATTENTION: this is vital to make WiFi connection to EEE work
	# https://www.raspberrypi.org/forums/viewtopic.php?t=247310
	# also here (last entry)
	# 
	#
	# add to file: /etc/dhcpcd.conf
	# HSLUEEEFIX: WPA2 enterprise WLAN bug fixed
	filename = "/etc/dhcpcd.conf"
	if not is_pattern_in_file( filename, "HSLU_EEE_FIX" ):
		append_to_file( filename, \
						"# HSLU_EEE_FIX: fixed WPA2 enterprise WLAN bug\n" \
						+ "interface wlan0\n" \
						+ "env ifwireless=1\n"
						+ "env wpa_supplicant_driver=wext,nl80211\n" )
		print( "[INFO] fixed WPA2 enterprise WLAN bug in file: {}".format( filename ) )
	else:
		print( "[INFO] WPA2 patch already applied to file: {}".format( filename ) )

		
#--------------------------------------------------------------------------------------------	
def do_users() -> None:
    print_sep_line()
    username = "admin"
    password = "admin"
    print("creating a new user - admin")
    try:
        user_check.getpwnam(username)
    except KeyError:
        bash_cmd("sudo useradd -m -p " + password + " -s /bin/bash " + username, check=True, shell=False)
    bash_cmd("sudo usermod -a -G sudo " + username, check=True, shell=False)
    bash_cmd("sudo echo " + username + ":" + password + " | chpasswd", check=True, shell=False)

	
#--------------------------------------------------------------------------------------------	
def do_parse_wpa_supplicant():

	with open( "/etc/wpa_supplicant/wpa_supplicant.conf", "rt" ) as f:
		data = f.read()

	print( data )

	others = []
	networks = {}
	
	PRINTME = False
	for line in data.split("\n"):
		line = line.lstrip()
		#print( "??", line, "??" )
		if PRINTME:
			networkLine += line + "\n"
			#print( line ) 			
		else:
			PRINTME = line.startswith( "network" )
			networkLine = line + "\n"
			if not PRINTME:
				others.append( line )

		# found closing } -> stop parsing this network
		if line.find( "}" ) >= 0:
			PRINTME = False
			
			#networkLine += line + "\n"
			sx = networkLine.find( "ssid" )
			ex = networkLine.find( "\n", sx )
			if sx == -1 or ex == -1:
				continue
			key = networkLine[ sx:ex ]
			sx = key.find( "\"" )
			ex = key.find( "\"", sx+1 )
			if sx == -1 or ex == -1:
				continue
			key = key [sx+1:ex ]
			#print( key, "->", networkLine ) 
			networks[ key ] = networkLine

	#print( others )
	#print( networks )
	return networks


#--------------------------------------------------------------------------------------------
def tabify( network ):

	network = network.replace("\n","\n\t")

	# correct last newline & tab
	ex = network.find("}")
	if ex >= 0:
		trail = network[ex:].replace("\n\t","\n")
		network = network[0:ex] + trail

	return network


#--------------------------------------------------------------------------------------------	
def do_hslu_eee_wlan() -> None: # identity: str, password: str) -> None:
	global stepCounter
	print_sep_line()
	print( "{}. Configuring raspi for HSLU's EEE-network:\n".format( stepCounter ) )
	stepCounter += 1

	wpa_supp_file = "/etc/wpa_supplicant/wpa_supplicant.conf"
	print( "Attention!\nThis step will erase any existing configuration in {}. \nIn particular it will overwrite any configuration for your home WiFi, but you can always recreate this part later.".format( wpa_supp_file ) )
	if not consent():
		print( "[INFO] skipping configuration for HSLU's EEE-network." )
		return None

	# add HSLE EEE network configuration to wpa_supplicant
	SSID_LINE = "ssid=\"EEE\"\n"
	if is_pattern_in_file( wpa_supp_file, SSID_LINE ):
	
		print("[WARN] EEE network configuration already exists in {}.".format( wpa_supp_file ) )
		print("Do you want to overwrite existing EEE network configuration?")
		if not consent():
			print("[INFO] skipping EEE configuration." )
			return None

	eeeHostName, eeePassword, eeeIdentity  = get_hslu_eee_credentials()

	eeeConfig = "network={\n" \
		+ SSID_LINE \
		+ "scan_ssid=1\n" \
		+ "key_mgmt=WPA-EAP\n" \
		+ "identity=\"{0}\"\n".format( eeeIdentity ) \
		+ "password=\"{0}\"\n".format( eeePassword ) \
		+ "phase1=\"peaplabel=0\"\n" \
		+ "phase2=\"auth=MSCHAPV2\"\n" \
		+ "priority=1\n}\n"
	print( "--- EEE network ---\n", eeeConfig, "\n---\n" )
	networks = do_parse_wpa_supplicant()
	networks[ "EEE" ] = eeeConfig
	print( networks )
	
	# create new wpa_supplicant.conf file
	print("Creating new EEE network configuration in: " + wpa_supp_file)
	wpa_config = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n" \
		+ "update_config=1\n" \
		+ "country={}\n\n".format( RASPI_WIFI_COUNTRY_CODE )
	for network in networks.values():
		wpa_config += tabify( network )

	# write wpa_supplicant with EEE configuration
	with open( wpa_supp_file, "wt" ) as wifi:
		wifi.write( wpa_config )

		
	# set WiFi country code
	bash_cmd("sudo iw reg set {}".format( RASPI_WIFI_COUNTRY_CODE ), check=True, shell=False)
	# enable WiFi wireless interface ( usually wlan0 )
	bash_cmd("sudo rfkill unblock wifi", check=True, shell=False)

	# set time/date zone
	bash_cmd("sudo timedatectl set-timezone Europe/Zurich", check=True, shell=False)

	# give raspi some time to adjust
	waitSec = 10
	print( "\nWaiting for {} seconds before applying new WiFi settings.".format( waitSec ) )
	time.sleep( waitSec )
	
	# apply new WiFi settings
	print( "Note: wpa_cli takes some time. Please, be patient." )
	bash_cmd("wpa_cli -i {} reconfigure".format( networkDevice ), check=True, shell=False)
	bash_cmd("sudo systemctl restart wpa_supplicant.service", check=True, shell=False)
	bash_cmd("sudo systemctl restart networking.service", check=True, shell=False )
	return eeeHostName


#--------------------------------------------------------------------------------------------
def add_home_network( ssid, password ):

	rc, homeConfig = run_command( "wpa_passphrase \""+ssid+"\" \""+password+"\"" )

	networks = do_parse_wpa_supplicant()
	networks[ ssid ] = homeConfig
	print( networks )
	
	# create new wpa_supplicant.conf file
	wpa_supp_file = "/etc/wpa_supplicant/wpa_supplicant.conf"
	print("Creating new home WiFi network configuration in: " + wpa_supp_file)
	wpa_config = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n" \
		+ "update_config=1\n" \
		+ "country={}\n\n".format( RASPI_WIFI_COUNTRY_CODE )
	for network in networks.values():
		wpa_config += tabify( network )

	# write wpa_supplicant with EEE configuration
	with open( wpa_supp_file, "wt" ) as wifi:
		wifi.write( wpa_config )


#--------------------------------------------------------------------------------------------
def option_add_home_network():

	print_sep_line()
	print( "Would you like to create a network config to connect to your home WiFi?" )
	if not consent():
		return False

	ssid = input("Home WiFi network name (SSID): ")
	password = input("Home WiFi network pasword: ")
	add_home_network( ssid, password )
	return True


#--------------------------------------------------------------------------------------------	
def do_hostname( hostname: str ) -> None:
	
	print_sep_line()

	# https://raspberrypi.stackexchange.com/questions/75901/raspberry-pi-uses-local-not-fqdn
	identity = hostname.split(".")[ 0 ]
	bash_cmd( "hostnamectl set-hostname {}".format( identity ), check=True, shell=False )

	# change entry in /etc/hosts to FQDN and alias (aka identity)
	filename = "/etc/hosts"
	print("setting hostname in file {} to {}.".format( filename, hostname ) )
	with open( filename, "rt" ) as fin:
		data = fin.read()

	IPLOCALHOST = "127.0.1.1"
	ix = data.find( IPLOCALHOST )
	if ix >= 0:
		ex = data.find("\n",ix)
		if ex >= 0:
			# https://unix.stackexchange.com/questions/186859/understand-hostname-and-etc-hosts
			data = data[ 0:ix ] + "{} \t {}".format( IPLOCALHOST, identity ) + data[ ex:len(data) ]

	with open( filename, "wt") as fout:
		fout.write( data )
					
	# src: https://www.howtogeek.com/167195/how-to-change-your-raspberry-pi-or-other-linux-devices-hostname/

	# commit the changes to the system
	# https://www.raspberrypi.org/forums/viewtopic.php?t=135126
	#bash_cmd( "sudo hostname {}".format( identity ), check=True, shell=False )


	# Tips & tricks
	# sudo /etc/init.d/avahi-daemon restart
	# Enable wpa_supplicant:
	# systemctl enable wpa_supplicant.service
	# systemctl start wpa_supplicant.service
	# Restart wifi networking
	# sudo wpa_cli -i wlan0 reconfigure
	# Restart networking:
	# systemctl restart dhcpcd.service

	# "Manually" retrieve ip address via dhcp
	# sudo dhclient -v wlan0
	
	# Status:
	# service --status-all

	# wlan0 down & up
	# sudo ip link set dev wlan0 down
	# sudo ip link set dev wlan0 up

	# check if WiFi received address
	# ip -c add


#--------------------------------------------------------------------------------------------
def is_internet_reachable():

	rc, output = run_command( "ping 8.8.8.8 -c 1 -W 100" )
	print( rc, output )
	return ( rc == 0 )

	
#--------------------------------------------------------------------------------------------
def do_raspi_system_update():

	print( "[INFO] performing system update. This requires internet connection." )
	if not is_internet_reachable():
		
		print( "[WARN] internet is *not* reachable. Skipping raspi system update." )
		return

	# apt-get quiet -> https://peteris.rocks/blog/quiet-and-unattended-installation-with-apt-get/
	print( "[INFO] running update" )
	rc, output = run_command( "sudo apt-get update --yes" )
	print( "[INFO] running upgrade. This may take several minutes." )
	rc, output = run_command( "sudo apt-get upgrade --yes" )
	print( "[INFO] running full upgrade. This may take several minutes." )
	rc, output = run_command( "sudo apt-get full-upgrade --yes" )
	print( "[INFO] system upgrade complete" )
	return

	
#--------------------------------------------------------------------------------------------
def do_ibm_requirements():

	print( "[INFO] performing Node Red installation. This requires internet connection." )
	if not is_internet_reachable():
		
		print( "[WARN] internet is *not* reachable. Skipping Node Red installation." )
		return
	
	rc, output = run_command( "sudo apt install build-essential git curl" )
	#os.system( "bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)" )
	rc, output = run_command( "curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered --output update-nodejs-and-nodered" )
	rc, output = run_command( "chmod +x update-nodejs-and-nodered" )
	rc, output = run_command( "su -l pi ./update-nodejs-and-nodered  --confirm-install  --confirm-pi" )


	# Note: raspi allows installation of node-red, albeit without npm via apt-get. This is *strongly* *not* recommended.
	#
	# check versions with:
	# node -v
	# npm -v
	# node-red -v
	
	# autostart nodered ( if desired)
	# sudo systemctl enable nodered.service
	
	# disable autostart
	# sudo systemctl disable nodered.service

	# opening editor
	# If you are using the browser on the Pi desktop, you can open the address: http://localhost:1880.
	# When browsing from another machine you should use the hostname or IP-address of the Pi:
	# http://<hostname>:1880. You can find the IP address by running hostname -I on the Pi.

	# tutorial
	# https://nodered.org/docs/tutorials/first-flow
	
	
#--------------------------------------------------------------------------------------------	
def do_disable_power_management():
	# from: https://desertbot.io/blog/headless-raspberry-pi-4-ssh-wifi-setup

	# check power management status with
	# iwconfig
	# or
	# /sbin/iw wlan0 get power_save

	# after applying this. it should be OFF
	
	# in /etc/rc.local
	# replace last line: exit 0 with
	# /sbin/iw wlan0 set power_save off
	# exit 0
	print( "[INFO] checking whether WiFi power management il already disabled." );
	filename = "/etc/rc.local"
	if not is_pattern_in_file( filename, "# HSLU_POWER_MANAGEMENT FIX" ):
		if replace_in_file( filename, \
							"exit 0", \
							"# HSLU_POWER_MANAGEMENT FIX\n/sbin/iw wlan0 set power_save off\nexit 0\n" ):
			print( "[INFO] set WiFi power management to disabled." )
	else:
		print( "[INFO] WiFi power management has already been disabled." )

	pass
	
	
#--------------------------------------------------------------------------------------------	
def do_reboot() -> None:
	print( "[INFO] system is rebooting now." )
	#os.system("sudo shutdown -r now") # raspi does not reboot, but halts. Variant (-t9) circumvents this problem
	os.system("sudo shutdown -t9 -r now")

	
#--------------------------------------------------------------------------------------------	
def do_shutdown() -> None:
	
	print( "[INFO] system is shutting down now." )
	os.system("sudo shutdown -h now")


#--------------------------------------------------------------------------------------------	
def get_device_from_cmd_line( args, defaultDevice ):

	if len( sys.argv ) > 1:
		networkDevice = sys.argv[1]
	else:
		networkDevice =  defaultDevice
	return networkDevice


#--------------------------------------------------------------------------------------------	
def get_hostname():
	
	return socket.gethostname()


#--------------------------------------------------------------------------------------------	
def is_fresh_install():
	
	return IS_FRESH_INSTALL


#--------------------------------------------------------------------------------------------	
def is_run_within_interpreter( firstArg ):
	
	return not firstArg.startswith("./")


#--------------------------------------------------------------------------------------------	
def is_run_with_admin_rights( args ):
	
	if not is_user_admin():
		dummy = " ".join( args )
		# add interpreter if called in such a way
		if is_run_within_interpreter( args[ 0 ] ):
			dummy = "python3 "+dummy
		print( "\n[ERROR] This script needs to modify system settings. This is only possible when run with admin priviliges. Run again with 'sudo' as follows:\n\n         sudo {}\n\n".format( dummy ) )
		return False
	else:
		return True

	
#--------------------------------------------------------------------------------------------	
def consent_reinstallation():

	oldHostName = get_hostname()
	print_sep_line()
	print( "[WARNING] This script is run on an already modified setup of raspberry.\n           You may have run this script already on this raspi.\n           Would you like to overwrite existing configuration?\n" )
	return consent()


#--------------------------------------------------------------------------------------------	
def notification_new_hostname( hostName, oldHostName ):

	print_sep_line()
	print( "Attention!" )
	print( "Note that the raspi hostname has been changed to:\n{}\n".format( highlight( hostName ) ) )
	print( "This raspi will no longer respond to {}, but only to: ".format( oldHostName ) )
	print( "+ HSLU-EEE network: {}\n".format( hostName ) )
	print( "+ Ethernet direct connection: {}\n".format( hostName.split(".")[0]+".local" ) )
	print()
	print ( "Press any key to continue ..." )
	wait_key ()
	

#--------------------------------------------------------------------------------------------	
if __name__ == '__main__':
	
	if not is_run_with_admin_rights( sys.argv ):
		sys.exit( -1 )
	
	print_sep_line()
	print_banner()

	if ( len( sys.argv ) > 1 ) and ( sys.argv[1] == "ibm" ):

		do_raspi_system_update()
		do_ibm_requirements()
		sys.exit( 0 )
	
		
	if not is_fresh_install():
		if not consent_reinstallation():
			sys.exit(-1)
		
	if not do_mac_address( get_device_from_cmd_line( sys.argv, DEFAULT_NETWORK_DEVICE ) ):
		sys.exit( -1 )

	do_vnc()
	do_camera()
	do_wpa_fix()
	do_disable_power_management()
	do_users()
	
	hostName = do_hslu_eee_wlan() # identity = ide, password = pwd )
	option_add_home_network()
	
	if hostName:
		do_hostname( hostname = hostName )
		notification_new_hostname( hostName, oldHostName )

	waitSec = 10
	print( "\nWaiting for {} seconds ...".format( waitSec ) )
	time.sleep( waitSec )

	#do_reboot()

