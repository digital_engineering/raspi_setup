#!/usr/bin/env python3
"""
Author: Björn Jensen
Date: 19.10.2021
Description: Installation of node red and requirements for IBM workshop.
"""
import os
import subprocess
import sys
import pwd as user_check
import time
import shlex
import ctypes
import socket


#--------------------------------------------------------------------------------------------	
class AdminStateUnknownError(Exception):
    """Cannot determine whether the user is an admin."""
    pass


#--------------------------------------------------------------------------------------------	
def is_user_admin():
    # type: () -> bool
    """Return True if user has admin privileges.

    Raises:
        AdminStateUnknownError if user privileges cannot be determined.
    """
    try:
        return os.getuid() == 0
    except AttributeError:
        pass
    try:
        return ctypes.windll.shell32.IsUserAnAdmin() == 1
    except AttributeError:
        raise AdminStateUnknownError


#--------------------------------------------------------------------------------------------	
def wait_key():
    ''' Wait for a key press on the console and return it. '''
    result = None
    if os.name == 'nt':
        import msvcrt
        result = msvcrt.getch()
    else:
        import termios
        fd = sys.stdin.fileno()

        oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)

        try:
            result = sys.stdin.read(1)
        except IOError:
            pass
        finally:
            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)

    return result


#--------------------------------------------------------------------------------------------	
def highlight( txt ):
	return ">>> "+txt+" <<<"


#--------------------------------------------------------------------------------------------	
def print_sep_line() -> None:
	print("\n-----------------------------------------------------------------------------\n")


	#--------------------------------------------------------------------------------------------	
def print_banner() -> None:
	"""
	use -> https://patorjk.com/software/taag/#p=display&f=Slant&t=RASPI%20CONFIG
	"""
	banner = """
    HSLU Digital Engineering - Workshop Setup Tool
	"""
	print( banner )
	print_sep_line()
	print()
	print( "Press any key to continue ..." )
	wait_key ()


#--------------------------------------------------------------------------------------------	
def bash_cmd(bash_str: str, check=True, shell=False) -> subprocess.CompletedProcess:
	try:
		if BASH_VERBOSE:
			print( "> {}".format( bash_str ) )
			process = subprocess.run(bash_str.split(" "), check=check, shell=shell)
			return process
	except subprocess.CalledProcessError as e:
		print(e)
		sys.exit(1)


#--------------------------------------------------------------------------------------------	
def run_command(command) -> tuple:
	process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
	output = ""
	while True:
		line = process.stdout.readline()
		if ( len( line ) == 0 ) and ( process.poll() == 0 ): #is not None:
			break
		if line:
			#print( line.strip() )
			output += line.decode("utf-8").strip() + "\n"
	rc = process.poll()
	return rc, output


#--------------------------------------------------------------------------------------------
def is_internet_reachable():

	rc, output = run_command( "ping 8.8.8.8 -c 1 -W 100" )
	print( rc, output )
	return ( rc == 0 )


#--------------------------------------------------------------------------------------------	
def is_run_within_interpreter( firstArg ):
	
	return not firstArg.startswith("./")


#--------------------------------------------------------------------------------------------	
def is_run_with_admin_rights( args ):
	
	if not is_user_admin():
		dummy = " ".join( args )
		# add interpreter if called in such a way
		if is_run_within_interpreter( args[ 0 ] ):
			dummy = "python3 "+dummy
		print( "\n[ERROR] This script needs to modify system settings. This is only possible when run with admin priviliges. Run again with 'sudo' as follows:\n\n         sudo {}\n\n".format( dummy ) )
		return False
	else:
		return True

	
#--------------------------------------------------------------------------------------------
def do_raspi_system_update():

	print( "[INFO] performing system update. This requires internet connection." )
	if not is_internet_reachable():
		
		print( "[WARN] internet is *not* reachable. Skipping raspi system update." )
		return

	# apt-get quiet -> https://peteris.rocks/blog/quiet-and-unattended-installation-with-apt-get/
	print( "[INFO] running update" )
	rc, output = run_command( "sudo apt-get update --yes" )
	print( "[INFO] running upgrade. This may take several minutes." )
	rc, output = run_command( "sudo apt-get upgrade --yes" )
	print( "[INFO] running full upgrade. This may take several minutes." )
	rc, output = run_command( "sudo apt-get full-upgrade --yes" )
	print( "[INFO] system upgrade complete" )
	return

	
#--------------------------------------------------------------------------------------------
def do_ibm_requirements():

	print( "[INFO] performing Node Red installation. This requires internet connection." )
	if not is_internet_reachable():
		
		print( "[WARN] internet is *not* reachable. Skipping Node Red installation." )
		return
	
	rc, output = run_command( "sudo apt install build-essential git curl" )
	#os.system( "bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)" )
	rc, output = run_command( "curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered --output update-nodejs-and-nodered" )
	rc, output = run_command( "chmod +x update-nodejs-and-nodered" )
	rc, output = run_command( "su -l pi ./update-nodejs-and-nodered  --confirm-install  --confirm-pi" )


	# Note: raspi allows installation of node-red, albeit without npm via apt-get. This is *strongly* *not* recommended.
	#
	# check versions with:
	# node -v
	# npm -v
	# node-red -v
	
	# autostart nodered ( if desired)
	# sudo systemctl enable nodered.service
	
	# disable autostart
	# sudo systemctl disable nodered.service

	# opening editor
	# If you are using the browser on the Pi desktop, you can open the address: http://localhost:1880.
	# When browsing from another machine you should use the hostname or IP-address of the Pi:
	# http://<hostname>:1880. You can find the IP address by running hostname -I on the Pi.

	# tutorial
	# https://nodered.org/docs/tutorials/first-flow


	

#--------------------------------------------------------------------------------------------	
if __name__ == '__main__':
	
	if not is_run_with_admin_rights( sys.argv ):
		sys.exit( -1 )
	
	print_sep_line()
	print_banner()


	do_raspi_system_update()
	do_ibm_requirements()

