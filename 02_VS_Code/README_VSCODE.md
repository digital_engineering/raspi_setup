[[_TOC_]]

# VS CODE SETUP
Dieses ReadMe ist für das Aufsetzen und Inbetriebnehmen von VS Code für das Modul Python Grundlagen.


## Installieren von Extensions

Suchen Sie dazu unter dem Tab `Extensions/` nach  `Jupyter`: 

![Jupyter Extension](screenshots/jupyter_extension.PNG)


# VS Code Setup
1. Laden Sie VS Code herunter: [VS Code Download Seite](https://code.visualstudio.com/download)
2. Installieren Sie VS Code für OS
3. Öffnen sie VS Code
4. Erstellen Sie ein File namens `hello_world.py`
5. Akzeptieren Sie den Pop-Up (unten rechts) und installieren Sie die Extentions für Python

![Select_Powershell](/03_VS_Code/screenshots/vs_code_pop_up.PNG)

6. Wählen Sie Powershell als Terminal Konsole aus:
   1. Drücken Sie `CTRL + SHIFT+ P`
   2. Suchen Sie nach `Terminal: Select Default Profile`
   3. Wählen Sie Powershell aus

![Select_Powershell](/03_VS_Code/screenshots/vs_code_powershell.PNG)

10. Wählen Sie einen ihren `conda (base)`Interpreter als Python Interpreter aus:
    1. Drücken Sie `CTRL + SHIFT+ P`
    2. Suchen sie nach `Python: Select Interpreter`
    3. Wählen Sie ihren `(base)` Interpreter von Anaconda

## Fehlermeldung (roter Eintrag im Terminal)
1. Falls Sie eine rote Fehlermeldung bekommen, müssen Sie die Pfade zu den Files  (`conda.exe`) und Pyhton (`python.exe`) zu `PATH` hinzufügen
- Diese Files sollten unter `C:\Users\ProgramData\Anaconda\Scripts` 
- oder unter `C:\Users\anaconda3\Scripts` zu finden sein. (`Users` mit Ihrem Benutzernamen ersetzen). 2
2. Speichern Sie die gefundenn Pfade (ohne conda.exe  und ohne python.exe) in einem .txt File temporär ab.

### Ich finde die Files nicht: 
1. öffnen sie ihre **Anaconda Promt**:

![Anaconda Promt](/03_VS_Code/screenshots/anaconda_promt.PNG)

2. Und geben Sie `where pyhton` eingeben: 

![Where Python](screenshots/where_python.PNG)

3. Conda sollte dann im Unterodner `/Scripts/` zu finden sein.
4. Speichern Sie die gefundenn Pfade (ohne conda.exe  und ohne python.exe) in einem .txt File temporär ab.
5. Suchen Sie nach **Umgebungsvariablen**:

![umgebungsvar1](screenshots/umgebungsvariablen.PNG)

### Pfade in `path` hinzufügen:
1. Öffnen Sie bei den Benutzerdefinierten Variablen **(oberes Feld)** `Path` mit **bearbeiten**

![umgebungsvar2](screenshots/umgebungsvariablen_2.PNG)

2. Fügen Sie hier ihre die Pfade von conda.exe und python.exe mit `neu` hinzu **(ohne filenames!)**
    1. Diese sollten `C:\Users\ProgramData\Anaconda\Scripts` für Anaconda (conda.exe)
    2. Und `C:\Users\anaconda3` für Python (python.exe) sein. 

![umgebungsvar3](screenshots/umgebungsvariablen_3.PNG)

3. Öffnen Sie nun Powershell als **Administrator** und geben Sie `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned ` ein
4. öffnen sie ihre **Anaconda Promt**:

![Anaconda Promt](/03_VS_Code/screenshots/anaconda_promt.PNG)

5.  und geben Sie den Befehl ein: `conda init powershell `
6. Öffnen Sie wieder VS Code und Schliessen Sie ihr Terminal

## Testen von VS Code
1. Öffnen Sie ein neues Temrinal (Taskliste oben, Terminal / new Terminal)
2. Erstellen Sie ein `hello_world.py`File (mit dem entprechenden `print` statement)
3. Führen Sie ihr hello_world.py file mit `python hello_world.py` aus dem Terminal aus.
4. Versuchen Sie auf das Skript auch via den Ausführen knopf (Dreieck - oben rechts) auszuführen
5. Herzlichen Glückwunsch, Sie haben ein Hello World in VS Code ausgeführt!

## Extensions installieren
Wir installieren folgende VS Code Extensions für VS Code:
1. `Remote SSH` Extension
   1. Suchen Sie dazu unter dem Tab `Extentions/` nach `Remote SSH`:

![VS_Code_Remote_SSH_Extention](/03_VS_Code/screenshots/vs_code_extensions.PNG)

2. Installieren Sie diese
3. Suchen sie nach `Jupyter`: 

![Jupyter Extension](screenshots/jupyter_extension.PNG)

4. Installieren sie auch diese Extension

### SSH Extension
Mit der SSH Extension kann in der VSCode IDE direkt auf dem Raspberry Pi gearbeitet werden.

1. SSH Verbindung konfigurieren mit hostname und username (im Beispiel: Hostname=ubuntu, User=air (Verbinden über IP Adresse hatte nicht funktioniert)):
![Konfiguration](screenshots/remote_explorer_new_connection.png)

2. Remote Dateien sind in VSCode  verfügbar:
![Dateien](screenshots/file_explorer_remote.png)

3. Es können mehrere (ssh) Terminals gleichzeitig aus VSCode geöffnet werden:
![SSH Terminals](screenshots/remote_terminal.png)

## Troubleshooting
### "current working directory" / Ausführverzeichnis / relative Pfade
Wenn ein Skript relative Pfade verwendet, die sich auf das Verzeichnis beziehen, in welchem das Skript abgelegt ist, wird, wenn nichts anderes konfiguriert ist, das Verzeichnis des `Terminal` Windows verwendet! 
Man kann im `Terminal` also bei Bedarf zum passenden Pfad navigieren mit (`cd <pfad>`).

Das Ausführ-Verzeichnis (aka current working directory) im Screenshot unten ist `C:\Users\tablaas\source\vscode\projects\air_perception`.

![cwd](screenshots/vscode_cwd.jpg)

