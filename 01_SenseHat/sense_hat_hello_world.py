import sense_hat

if __name__ == '__main__':
    print("Sense Hat Version", sense_hat.__version__)
    sense = sense_hat.SenseHat()
    sense.rotation = 180  # Display-Rotation
    sense.show_message("Hallo Sense Hat!")
