# Sense Hat Setup

Um das Sense Hat zu aufzusetzen, müssen Sie zu dieses zuerst montieren.

## Montage Vorbereitungen
1. Holen Sie sich vom Dozenten:
   - ein SenseHat
   - einen Schraubenzieher (Kreuz oder Schlitz, checken sie die mitgelieferten Schrauben)
   - Spitzzange (falls vorhanden)
2. Schalten Sie dafür das Raspi aus. Warten Sie dafür bis die grüne LED am Raspberry Pi nicht mehr blinkt.
3. Trennen Sie anschliessend die Stromversorgung von Ihrem Raspberry Pi.

## Eigentliche Montage

![Motage Gif](https://projects-static.raspberrypi.org/projects/rpi-sensehat-attach/24ce7d349ff9a1ee135f0fe39f75ec89aad7b98e/en/images/animated_sense_hat.gif)

4. Befolgen Sie dann die Sense-Hat Montage Anleitung auf: [Anleitung Montage SenseHat](https://projects.raspberrypi.org/en/projects/getting-started-with-the-sense-hat/2)
5. Stecken Sie das Raspi wieder ein und Verbinden Sie sich via `SSH` mit dem Raspi. (via VS Code oder Powershell) `ssh pi@eee-XXXX.simple.eee.intern` (XXXX = ihre eee - Nummer)

## Testen SenseHat
6. Laden Sie nun das `das Hello_WoldS_ense_Hat.py` auf das Raspi herunter. Führen Sie dafür folgenden Befehl aus: `curl --output sense_hat_hello_world.py --url https://gitlab.switch.ch/digital_engineering/raspi_setup/-/raw/00_Raspi/Dozenten_Setup_fawi/sense_hat_hello_world.py`
7. Geben Sie dann Befehl `ls` ein um den Ordner Inhalt zu sehen. 
8. Sie sollten nun das Python File `sense_hat_hello_world.py` in Ihrem Verzeichnis sehen.
9. Geben sie nun den Befehl `python3 sense_hat_hello_world.py` ein. Dies führt das Skript aus. Nun sollten die LEDs den Schriftzug `Hallo Sense Hat!` anzeigen.
10. Herzlichen Glückwunsch Sie haben das Sense-Hat erfolgreich in Betrieb genommen.
> HINWEIS: Falls Sie folgende Warnung in der Konsole erhalten haben, können Sie diese einfach ignorieren:

![Raspi_warning](/01_SenseHat/screenshots/warning_sense_hat_ignore.png)


# Arbeiten mit dem SenseHat
Wenn sie das SenseHat verwenden vollen müssen Sie zu erst dessen Library importieren:

````
from sense_hat import SenseHat
````
Anschliessend können Sie ein SenseHat Objekt erstellen, welches Sie verwenden können um:
- Sensoren auszulesen
- Text anzuzeigen
- Bilder anzuzeigen
- Daten zu speichern 
- etc


````
# erstellen SenseHat Objekt
sense = SenseHat()

# verwenden SenseHat objekt (für Text Anzeige)
sense.show_message("Hallo Sense Hat!")
````

Bei Interesse kann hier noch ein weiterführendes Tutorial gelesen werden: [Link Tutorial](https://raspberrytips.com/sense-hat-tutorial/)

# Arbeiten mit dem SenseHat Emulator

Es kann ebenfalls ein Online Emulator verwendet werden, um ein virtuelles Raspberry Pi mit SenseHat verwenden zu können.
Gehen Sie dazu auf [https://trinket.io/sense-hat](https://trinket.io/sense-hat)

Um den Emulator korrekt zu verwenden müssen sie nun das Emulierte SenseHat importieren (nicht das richtige SenseHat):

````
from sense_emu import SenseHat
````

