# Raspberry Pi Setup

Dieses Repo enthält Skripte und Erklärungen zum Einrichten des Raspberry Pi von Grund auf.
Es ist **NUR FÜR DOZENTEN UND ASSISTENTEN** von Python Grundlagen gedacht. 
Befolgen Sie die Schritte genau und achten Sie auf Ihre Eingaben.


## Voraussetzungen 

Sie benötigen einen Laptop mit:
- LAN Anschluss
- SD-Karten Leser
- WLAN 

# Einmalige Vorbereitungen

## Material holen (für SenseHat montage)
Holen Sie sich folgendes Material:
- einen Schraubenzieher (Kreuz oder Schlitz, checken sie die mitgelieferten Schrauben von SenseHat)
- Spitzzange (falls vorhanden)
- SD Card Reader (falls keiner am Laptop)
- SD Card Adapter (micro zu mini)

## Herunterladen von Files (für Raspi Aufsetzten)
Es müssen 4 Files heruntergeladen werden. Wir verwenden dazu Powershell und den ``curl`` Befehl:

### Herunterladen des Raspi Setup - Skripts
Als Erstes muss das Raspi Setup Skript, welches das Aufsetzten des Raspis vereinfacht, **EINMALIG** heruntergeladen werden:
Öffnen Sie dazu eine Powershell-Instanz.
1. Navigieren Sie in einen Ordner in den Sie das Setup-Skrip herunterladen wollen
2. Öffnen sie hier Powershell / CMD 
3. Geben Sie \
`curl.exe --output raspi_setup.py --url https://gitlab.switch.ch/digital_engineering/raspi_setup/-/raw/master/files/raspi_setup.py` \
ein und drücken Sie die ENTER Taste.

![Powershell_Download_PyScript](screenshots/PowerShell_PyScriptDownload.PNG)

4. Inspizieren sie kurz das ``.py`` file

![Raspi_Setup_File](screenshots/raspi_setup_file.PNG)


### Herunterladen Sense_Hat Hello_World
Für den Test des SensHats muss ebenfalls noch ein ``Hello_world.py`` file heruntergeladen werden.
1. Navigieren Sie in einen Ordner in den Sie das ``Hello_world.py``  herunterladen wollen
2. Öffnen sie hier Powershell / CMD
3. Geben Sie `curl.exe --output sense_hat_hello_world.py --url https://gitlab.switch.ch/digital_engineering/raspi_setup/-/raw/master/files/sense_hat_hello_world.py` ein.
4. Inspizieren sie kurz das ``.py`` file

![sense_hat_hw](screenshots/sense_hat_hello_wor.PNG)


### Herunterladen des Raspi Imagers
1. Gehen Sie auf [raspberrypi.org](https://www.raspberrypi.org/software/)
2. laden Sie den Raspberry Pi Imager herunter und installieren Sie ihn auf Ihrem PC.


# Eigentliches Aufsetzten Raspi

Wenn Sie wissen, was sie machen, können Sie alternativ diese kompakte **Befehlsliste** verwenden. [LINK FIle](https://gitlab.switch.ch/digital_engineering/raspi_setup/-/blob/master/00_Raspi/Dozenten_Setup/commands_compact.txt?ref_type=heads)
- Diese enthält **nur** die Befehle, welche nacheinander ausgeführt werden müssen.
- **Nicht verwenden, wenn sie das Setup nie gelesen haben**


## Herunterladen des Raspi Imagers
1. Mit der neuen
2. Gehen Sie auf [raspberrypi.org](https://www.raspberrypi.org/software/)
3. laden Sie den Raspberry Pi Imager herunter und installieren Sie ihn auf Ihrem PC.



## Eigentliches Aufsetzten Raspi

## SD Karte formatieren 
Als erstes müssen wir das korrekte OS auf die SD Karte laden und einen Benutzer erstellen. Dafür verwenden wir den Raspi Imager.
1. Öffnen Sie den Raspi Imager
2. Wählen Sie in der Raspberry Pi Imager Software das unter **Raspberry Pi Os (other** das **Raspberry Pi OS (legacy)** als Betriebssystem und die eingelegte SD-Karte als Speicher.
Hinweis: Am 10.10.23 wurde ein neues OS hochgeladen welches das Setup-Skript brickt.

![Raspi Imager](screenshots/legacy_os_0.PNG)

![Raspi Imager](screenshots/legacy_os.PNG)

5. Klicken Sie auf &#x2699; um die **Einstellungen zu öffnen.**
6. Stellen Sie folgende Einstellunge ein:
7. **Aktivieren** sie SSH. 

![Raspi_Imager_einstellunge_1](screenshots/sd_format_1.PNG)

8. Geben Sie als Benutzernamen: `pi` ein
9. Geben Sie als Passwort `raspberry` ein.

![Raspi_Imager_einstellunge_2](screenshots/sd_format_2.PNG)

10. Aktivieren Sie das WLAN. Geben Sie als SSID: `test_01`, mit Passwort: `12345678` ein. (kein effektives WLAN)
11. Wählen Sie das Schweizer Tastatur Layout (CH).
12. Drücken Sie auf `speichern` und `schreiben` um die SD-Karte zu formatieren. (Dies dauert einige Minuten)

![raspi_imager_loading](screenshots/legacy_os_3.PNG)

13. Werfen Sie im Anschluss die SD-Karte aus, entfernen sie aus Ihrem PC und stecken Sie sie zurück in den Raspberry Pi.


##  Raspi Einschalten
1. Stecken Sie die SD-Karte ins Raspi
2. Speisen Sie das Raspi mit dem USB-C Ladegerät
3. Verbinden Sie das Raspi mit ihrem PC (via LAN Kabel)
4. **Warten** Sie bis das Raspi nicht mehr gelb blinkt
- Dies dauert das erste Mal ziemlich lange (ca. 3min)
- Gelbe LED in der nähse des USB-C Steckers sollte in den letzten 20s **nicht mehr geblinkt haben**

## 3. Setup Raspi (via LAN Kabel)
In einem nächsten Schritt müssen sie sich (lokal) mit dem Raspi Verbinden. 
Aufgrund von Problemen beim Aufsetzen via WLAN, wird empfohlen die nächsten Schritte **nur** via **LAN** zu machen.
Die Schritte wurden auch **nur** für das Aufsetzten via **LAN** dokumentiert.

### Erster Verbindungs-Check
Beginnen Sie diesen Schritt erst, wenn das **gelbe LED** des Raspis **nicht mehr blinkt**. (Dies bedeutet, dass das Raspi noch am booten ist)
Versuchen wir das Raspi nun Anzupingen: 
1. Geben Sie in `Windows Powershell` (bei Mac heisst es Terminal) nun folgenden Befehl ein:
   - **ACHTUNG** Powershell (x86) **funktioniert nicht**, verwenden sie die normale Powershell version!
2. `ping raspberrypi.local`
3. Sobald Sie eine Antwort erhalten, können Sie mit dem nächsten Schritt fortfahren. 
   - **ACHTUNG:** bei MAC hört das pingen nicht auf. Wenn Sie Pakete empfangen haben, drücken Sie **COMMAND + C** um den Vorgang abzubrechen.

![Ping_Raspi_Local](screenshots/RaspiLocalPing.PNG)


### Files Kopieren
Wir wollen nun sowohl `raspi_setup.py` wie auch `sense_hat_hello_world.py` auf das Raspi kopieren
1. Navigieren Sie zum Ordner wo Sie diese Dateien heruntergeladen haben und öffnen Sie Powershell (Terminal für Mac)
2. Führen Sie folgenden Befehl aus um das Setup-Skript zu kopieren: `scp raspi_setup.py pi@raspberrypi.local:/home/pi`.
 - Das Passwort dazu ist `raspberry`.
 - **ACHTUNG**: das Passwort wird nicht angezeigt
 - **ACHTUNG**: Falls Sie die Tastatureinstellungen nicht auf CH geändert haben, ist das passwort evtl: `raspberrz`.

![Copy_Script](screenshots/raspisetup_to_pi.jpg)

- **Achtung:** Falls Sie folgende Fehlermeldung erhalten, müssen Sie in ihrem `known_hosts` File den Eintrag von ``raspberrypi.local`` **löschen**.

![DNS_Spoofing](screenshots/dns_spoofing.PNG)

- Dieses File finden Sie unter `Benutzer/.shh/known_hosts.txt`

![known_hosts](screenshots/known_hosts.PNG)

3. Nun müssen Sie ebenfalls das `sense_hat_hello_world.py` File mit folgendem Command kopieren: `scp sense_hat_hello_world.py pi@raspberrypi.local:/home/pi`

### Verbinden, Registrieren und Aufsetzen
Nun wollen wir uns mit dem Raspi via ssh verbinden, um das Raspi im **eee-Netz zu registrieren** und das ``raspi_setup.py`` **Script auszuführen**.
1. Verbinden Sie sich nun mit via Secure Shell (`ssh`) mit ihrem Raspi, benutzen Sie dazu folgenden Befehl `ssh pi@raspberrypi.local`
2. Anschliessend wird Sie Powershell Fragen, ob Sie die SSH verbindung erstellen wollen. Geben sie `Yes` ein und drücken Sie `ENTER`
3. Nun müssen Sie noch ihr Benutzer Passwort eingeben (Default Passwort von Benutzer `pi` ist ``raspberry``)
 - **ACHTUNG**: das Passwort wird nicht angezeigt
4. Wenn sie nun den Befehl ``ls`` eingeben, sollten sie die beiden Python Files (`sense_hat_hello_world.py` und `raspi_setup.py`) sehen:

![File_Check](screenshots/raspi_file_check.PNG)

5. Nun wollen Sir das eigentliche Raspi-Setup ausführen:
6. Geben Sie dafür `sudo python raspi_setup.py` ein. Möglicherweise müssen Sie das Passwort erneut eingeben, da Sie den Befehl als superuser (sudo) ausführen.
7. Drücken Sie dann irgend eine Taste um das Setup zu starten.
8. Notieren (kopieren) Sie sich die angegebene MAC Adresse:

![mac_address](screenshots/mac_adress.PNG)

7. Gerät registrieren:
- Falls Sie ein **Dozent** sind befolgen sie folgende Schritte:
- gehen Sie auf [EEE_Portal](https://eeeportal.hslu.ch/)
- Erstellen Sie einen neuen Eintrag für das Raspi im **EEE WLAN Simple Lab 014**.
- Fügen Sie die **MAC-Adresse** ein
- Geben Sie dem Raspi eine neue (noch nicht vorhandene) 4 -stellige Nummer
- Klicken Sie anschliessen auf **Gerät Erstellen**.

![eee-setup](screenshots/eee_creation.PNG)

8. Gerät registrieren (V2):
- Falls Sie ein **Assistent** sind befolgen Sie folgende Schritte:
- Schicken Sie **Fabian Widmer** die MAC Adresse via MS Teams Chat (zB `e4:5f:01:84:bc:f1`). Er registriert Ihr Raspi auf dem **eee-Portal** 
- Notieren Sie sich den **eee-Netz Hostnamen** und **eee-Passwort** welcher Fabian ihnen zurückschickt, zB:
- Username: ``eee-w014-001.simple.eee.intern``
- PW: ``zRZpAF7578C9rCZV9rv3``  (dies ist nicht ihr Raspi Passwort, sondern ihr **eee-Passwort**)
- **ACHTUNG:** kopieren sie die **Leerzeichen** hinter Passwort und Username **nicht!!!**.

12. **Wieder für alle**:
13. Drücken Sie wieder irgend eine Taste um im Setup weiter zu gehen.
14. Geben Sie jeweils `yes` ein. (dass Sie die Einträge überschreiben wollen)
15. Nun Frag Skript nach einem eee-Hostname: Geben Sie dazu den notierten **Hostname** aus dem EEE-Portal ein. (gesammter Hostname: `eee-XXXXX.simple.eee.intern`)
16. Geben Sie danach das angegebene **eee-Passwort** ein. (zb ``zRZpAF7578C9rCZV9rv3``)
- Achten Sie auf Leerschläge vor dem Passwort
17. Wenn das Skript sie nach einem Home-Wifi fragt, geben Sie ``no`` ein:

![home_wifi](screenshots/home_wifi.PNG)

18. Booten Sie das Raspi neu ``sudo reboot``
20. Trennen Sie die LAN Verbindung
22. Warten Sie bis das Raspi nicht mehr gelb blinkt (aufstarten)
 
## Verbinden via EEE-Netz (WLAN)
Um das Setup zu testen, versuchen Sie sich nun via **HSLU-WLAN** einzuloggen. 
1. Gehen Sie dazu mit ihrem Laptop ins **HSLU** WLAN.

![raspi_connection_2](screenshots/raspi_wlan.PNG) 

2. Für einen HSLU-Netz-Verbindungs-Check geben sie  `ping  eee-wXXX-XXX.simple.eee.intern` ein. **(XXX-XXX == ihre eee-ID)**
3. Falls Sie keine Verbindung bekommen, checken Sie, ob ihr Raspi einen Lease (Statische IP) bekommen hat.
- Falls Sie ein Assistent sind, fragen Sie einen Dozenten

![check_ip](screenshots/check_ip.PNG)

4. Verbinden Sie sich nochmals via SSH
- Verwenden Sie wenn möglich die **eee-Adresse** (`ssh pi@eee-wXXX-XXX.simple.eee.intern`)
- Ansonsten verwenden Sie die **statische IP** (`ssh pi@10.180.254.69`) 
- Fragen Sie einen Dozenten für diese Adresse, falls sie keinen Zugriff auf das EEE-Lab14 haben.
- Falls dies so ist, machen Sie mit dem  Kapitel [Statische_IP](#verbinden-mit-statischer-ip) weiter

### Verbinden mit Statischer IP
Es kann sein, dass das Setup Skript nicht richtig durchgelaufen ist und es deshalb Probleme mit der dynamischen IP gibt. 
1. Verbinden Sie sich mit der  **statischen IP** (`ssh pi@10.180.254.69`) via ``ssh`` mit dem Raspi.
2. Führen Sie deshalb das Setup Skript nochmals aus ``sudo python raspi_setup.py``
3. Checken Sie danach die WLAN einträge: ``sudo nano /etc/wpa_supplicant/wpa_supplicant.conf``
- Das File sollte danach etwa wie folgt aussehen: 

![WPA_Conf](screenshots/wpa_config.PNG)

3. Das File können Sie mit <kbd>CTRL</kbd>+<kbd>X</kbd> wieder schliessen
4. Rebooten Sie danach das Raspi ``sudo reboot``
5. Versuchen Sie sich mit der EEE-Adresse nochmals zu verbinden ``ssh pi@eee-wXXX-XXX.simple.eee.intern``
6. Wenn es immer noch nicht geht, fragen Sie **Fabian Widmer**.

## Weitere Raspi-Setup Einstellungen (manuell)
Befolgen Sie folgende Anleitungen
1. Verbinden Sie sich mit via EEE-Adresse mit dem Raspi  ``ssh pi@eee-wXXX-XXX.simple.eee.intern``
- alternativ verwenden Sie die Statische IP  (`ssh pi@10.180.XXX.XX`)

### S Code Bug-Fix
1. Deaktivieren Sie 64-bit auf dem Raspi. Führen Sie dazu: `sudo sh -c "echo 'arm_64bit=0' >> /boot/config.txt"` aus.
- Dies fügt die Zeile ``arm_64bit=0`` dem File `/boot/config.txt` hinzu (gegen VS Code Bug).

![sudo_echo](screenshots/sudo_echo.PNG)

### I2C Aktivieren
1. Vergewissern Sie sich ob auf ihrem Raspi das `I2C` Interface aktiviert ist (Für Sense Hat).
- Geben sie dazu den Befehl `sudo raspi-config nonint get_i2c` ein.
- Eine ``1`` bedeutet dabei dass `I2C` **deaktiviert** ist:

![I2C_deactivated](screenshots/ic2_deactivated.PNG)

2. Falls dies der Fall ist, müssen Sie `I2C` aktivieren.
3. Geben Sie dazu `sudo raspi-config nonint do_i2c 0` ein.
4. Rebooten sie nun das raspi mit dem Befehl `sudo reboot` 


## Dokumentieren der Raspi Daten
Wir wollen die Daten des Raspis dokumentieren.
1. Drucken Sie den Hostname ihres Raspis auf dem P-Touch **2-mal** aus und Kleben sie diese auf:
- Den LAN Port des Raspis
- Die Plastik Box des Raspis
2. Tragen Sie folgende Informationen in dieses Excel File ein: [Excel](https://hsluzern.sharepoint.com/:x:/s/PYTHONUnterricht-TM/EVgvfYFJWWpBv5U1KVCOav4B85AgJjn8ooH6mJpHr0QPog?e=CdjCfd)
- Raspi Nummer (`Raspi_0001`)
- Hostname (``eee-wXXX-XXX.simple.eee.intern``)
- MAC Adresse (`e4:5f:01:84:bc:f1`)
- EEE - Passwort (``zRZpAF7578C9rCZV9rv3``)

# Verbindung mit VS Code
Testen Sie kurz die Verbindung mit VS Code. 
1. Öffnen Sie VS Code
2. Gehen Sie zu **Remote Host**
3. Erstellen Sie einen Eintrag für einen **neuen Host**
- **Achtung:** Auch hier muss evtl wieder die Statische IP Verwendet werden, wenn diese vorher nicht verwendet werden konnte (30 min DHCP Retention)

![remote_host_entry](screenshots/ssh_new_host.PNG)

4. Drücken Sie auf **refresh** und wählen Sie dann **connect with Host in new window**

![ssh_connection_vs_code](screenshots/ssh_connect_vscode.PNG)

5. Wählen Sie ``Linux`` als OS an.
6. Geben Sie das Passwort (``raspberry``) ein.
7. Öffnen sie dn Ordner ``home/pi``.
8. Sie sollten nun die kopierten Files sehen und unten Links die Verbindung angezeigt bekommen:

![ssh_con_3](screenshots/ssh_connection_3.PNG)

- Falls Sie sich nun immer mit der **statischen** IP verbunden haben, versuchen sie es nochmals mit dem **eee-Hostname**
  **nach** dem SenseHat Setup und reboot.
9. Stecken Sie das Raspi aus (USB C).

# Sense Hat Setup
Das SenseHat ist der Sensor Aufsatz für das Raspberry Pi.

## Montage 
Um das Sense Hat zu aufzusetzen, müssen Sie zu dieses zuerst montieren: 
- Achten Sie dabei darauf, dass Sie die **Pins nicht verbiegen**.

![Motage Gif](https://projects-static.raspberrypi.org/projects/rpi-sensehat-attach/24ce7d349ff9a1ee135f0fe39f75ec89aad7b98e/en/images/animated_sense_hat.gif)

## Test SenseHat
1. Verbinden Sie sich wieder mit dem Raspi (via ``ssh`` oder VS Code)
2. Führen Sie das im vorhinein aufs Raspi geladene **Hello-World Skript** aus. `python sense_hat_hello_world.py  `
3. Falls nun **kein Schriftzug** über ihr LED-Display läuft, könnte das Sense-Hat defekt sein. 
- gehen Sie in diesem fall durch das folgende [Kapitel](#mgliche-fehler) durch:
4. Falls der Schriftzug erscheint, sind sie fertig **noice**.
5. Schalten Sie das Raspi aus ``sudo shutdown now``


### Mögliche Fehler
1. Überprüfen Sie, ob die ``sense-hat`` library installiert ist: ``pip show sense-hat``

![pip_show](screenshots/pip_show.PNG)

- Wenn diese **nicht installiert** ist, installieren sie diese: ``pip install sense-hat``
- dann Raspi rebooten & neu verbinden & nochmals testen

2. ``I2C`` ist nicht aktiviert:
- Siehe Kapitel [I2C Aktivieren](#problem-3-i2c-aktivieren)
- dann Raspi rebooten & neu verbinden & nochmals testen

3. Raspi forcieren SenseHat zu erkennen (scetchy)
- Dazu muss die Zeile ``dtoverlay=rpi-sense`` im File ` /boot/config.txt` angefügt werden.
- Verwenden Sie dazu folgenden Befehl:
- ``sudo sh -c "echo 'dtoverlay=rpi-sense' >> /boot/config.txt" ``
- dann Raspi rebooten & neu verbinden & nochmals testen

3. Sense Hat wechseln (last option)
- Wenn alles andere fehlgeschlagen ist, SenseHat wechseln
- defektes SenseHat markieren
- neues SenseHat montieren und neu probieren


# Mögliche Probleme beim Raspi Setup

## Problem 1: Fehlermeldung beim Verbinden - Could not Establish Connection 
Eine Fehlermeldung mit dem Titel `Could not establish connection` kann durch folgenden Fehler ausgelöst werden (siehe Bild).
Hierbei ist es wichtig zu prüfen, dass die Fehlermeldung durch den Fehler: `Ein Prozess hat versucht, zu einer nicht bestehenden Pipeline zu schreiben` (siehe Bild)

![Fehlermeldung_SSH_2](screenshots/ssh_problem_2.PNG)

Die Lösung dieses Problems geht folgendermassen:
1. Navigieren Sie in Windows zu ihrem **SSH - CONFIG FILE** dieses sollte unter `C:\Users\"IHR BENUTZERNAME" \.ssh` zu finden sein.

![SSH_Problem_2_1](screenshots/ssh_problem_2_1.PNG)

2. Öffnen Sie das File `config` mit einem Text Editor (zB Notepad)
3. Löschen Sie den Eintrag ihres Raspis darin
4. Schliessen Sie VS Code
5. Versuchen sie sich erneut zu Verbinden
6. Bei bestehendem Fehler fragen sie einen Dozenten oder Apotheker

## Problem 1: Verbinden funktioniert noch immer nicht:
**ACHTUNG:** Wenn Sie `sudo sh -c "echo 'arm_64bit=0' >> /boot/config.txt"` **mehrfach** ausgeführt haben, müssen sie das Boot/config.txt file manuell editieren und die doppel Einträge löschen.
- **ACHTUNG** sie Öffnen ein Einstellungs Files des Raspis -> **KEINE FEHLER JETZT**
- Wenn sie unsicher sind, fragen sie lieber einen Dozenten
1. Öffnen des Files: `sudo vi /boot/config.txt`
2. mit <kbd>&darr;</kbd> bis ans File Ende gehen.
3. mit Delte Taste <kbd>Del</kbd> Duplikate löschen
4. mit :wq! speichern und schliessen
5. -> kurze Kontrolle: `vi /boot/config.txt` -> ist nur noch ein Eintrag da?
6. -> wenn ja: `:q!` (schliessen ohne zu speichern)
7. Gehen Sie zurück ins VS Code und versuchen Sie sich nochmals zu verbinden (unter Tab `/remote`)
- Die erste Verbindung dauert ca 1-2min bis VS Code auf dem Raspi heruntergeladen und installiert ist


## Problem 3: I2C Aktivieren
1. Vergewissern Sie sich ob auf ihrem Raspi das `I2C` Interface aktiviert ist (Für Sense Hat).
- Geben sie dazu den Befehl `sudo raspi-config nonint get_i2c` ein.
- Eine ``1`` bedeutet dabei dass `I2C` **deaktiviert** ist:

![I2C_deactivated](screenshots/ic2_deactivated.PNG)

2. Falls dies der Fall ist, müssen Sie `I2C` aktivieren. Geben Sie dazu `sudo raspi-config` ein.
3. Darauf sollte sich folgender Promt öffnen:

![Interface Options](screenshots/i2c_2.PNG)

4. Wählen Sie die `Interface Options an`
5. Wählen Sie nun `I2C` aus und **aktivieren** Sie diesen.

![Raspi_I2C](screenshots/raspi-config-i2c.png) 

6. Schliessen Sie dann den config-promt indem sie auf **Finish** gehen:

![i2c_finish](screenshots/i2c_finish.PNG)

7. Rebooten sie nun das raspi mit dem Befehl `sudo reboot`