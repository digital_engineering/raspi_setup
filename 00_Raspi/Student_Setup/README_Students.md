# Anleitung zum ersten Verwenden des Raspberry Pis 
**ACHTUNG:** Dieses Repo enthält Skripte und Erklärungen zum Einrichten Ihres Raspberry Pi für den Kurs Python Grundlagen.
- Befolgen Sie diese Anleitungen genau und achten Sie auf ihre Eingaben in Kommandozeile und Terminal. 


<!---
- Generell gilt das Konzept: 

![Konzept](screenshots/konzept_3.jpg)
-->

**ALSO LOS GEHTS:** Das Rasperry Pi kann über verschiedenste Wege programmiert und angesteuert werden. Zwei diese Wege werden in diesem Kurs besprochen:
- via Kommando-Zeile 
- via VS Code

Sowohl die Kommando-Zeile wie auch VS Code verwendet das **SSH (Secure SHell)** Verfahren welches einen **REMOTE ZUGRIFF** auf ein Gerät erlaubt. Dafür benötigt man folgende Infromationen:
 - Device Adresse (``IP`` bzw ``Hostnamen``, sollte auf ihrem Raspi stehen (zB `eee-w014-001.simple.eee.intern`))
 - Benutzernamen auf dem Device (``pi`` für jedes neue Raspi)
 - Passwort (``raspberry`` für jedes neue Raspi)


Das Raspi wurde im **vorhinein schon in Betrieb genommen und getestet**, sodass sie dieses möglichst einfach verwenden können.
Falls Sie das Aufsetzten interessiert, können Sie hier mehr darüber lesen: [Anleitung_Raspi_Setup](https://gitlab.switch.ch/digital_engineering/raspi_setup/-/blob/master/00_Raspi/Dozenten_Setup/README_Dozent.md?ref_type=heads)

## Erste Lebenszeichen checken (via Kommandozeile)
Bevor wir uns jedoch mit dem Raspi verbinden, müssen wir sicherstellen, ob unser Laptop das Raspi im **WLAN** überhaupt sehen kann.
1. Stecken Sie das Raspi ein und warten Sie bis es **nicht mehr blinkt** (kleine LED neben SD Karte)
2. Melden Sie sich mit Ihrem Laptop im HSLU WLAN an.
3. Suchen Sie auf ihrerm Raspi-Karton den **Hostnamen** ihres Raspis (zB `eee-03268.simple.eee.intern`)  
4. Notieren Sie sich den Hostname in einem `text File  (.txt)`
5. Öffnen Sie Powershell (Windows) oder das Terminal (Mac / Linux).
- **ACHTUNG** Powershell (x86) **funktioniert nicht**, verwenden sie die normale Powershell version!
6. Testen Sie ob sie eine Verbindung mit ihrem Raspi haben. Verwenden Sie dazu den `ping` Befehl: `ping **Ihr_Raspi_Hostame**`.
- Kopieren Sie dazu ihren notierten Hostnamen in den Befehl.(zB: `ping eee-w014-XXX.simple.eee.intern`).
- Dies sendet ein paar Bytes an den Raspberry Pi über das Ethernet und prüft, ob er funktioniert und läuft.
7. Falls Sie eine Antwort bekommen sollten Sie den folgenden Promt sehen:
- **ACHUTNG** Bei Mac hört der PC nicht automatisch auf Packete zu senden. Drücken Sie dann einfach `Command + C`

![Ping_Raspi](screenshots/raspi_ping.PNG)

8. Falls Sie eine Antwort erhalten haben, war der erste Test erfolgreich
- schliessen dann Sie Powershell /Terminal.
- Ansonsten befolgen Sie das Kapitel [Verbindungsprobleme 1](#fehlermeldung-1).


## VS Code für Verbindung vorbereiten
Das eigentliche Verbinden mit dem RasperryPi wird via VS Code und `ssh` gemacht, da es das Programmieren vereinfacht.
Um sich via `ssh` verbinden zu können, müssen Sie folgende Extensions installieren:

1. Installieren sie die `Remote SSH` Extension in VS Code. Suchen Sie dazu unter dem Tab `Extensions/` nach `Remote SSH`:

![ssh_installation](screenshots/vs_code_extensions.PNG)

## Verbinden mit VS Code
Verbinden Sie sich nun mit Ihrem Raspi, indem Sie eine neue Remote erstellen:
1. Gehen Sie zum Tab `Remote Explorer` 
2. Drücken Sie auf `+` um sich mit einem neuen SSH Ziel zu verbinden

![VS_Code_ssh_setup](screenshots/vs_code_ssh.PNG)

3. Geben Sie `ssh pi@eee-w014-XXX.simple.eee.intern` ein.
  - Ändern Sie XXX für ihre Nummer

4. Wählen Sie das ``ssh_config`` als Speicherort für alle ihre SSH Verbindungen.

5. Öffnen Sie dann dieses File (indem Sie den Pop Up anclicken)
  - Alternativ können Sie auf das Zahnrad (⚙) bei SSH drücken um das File zu öffnen

![pop_up](screenshots/host_added.PNG)

- In ihrem config file sollten Sie nun einen solchen Eintrag haben:

```
Host eee-w014-XXX.simple.eee.intern
  HostName eee-w014-XXX.simple.eee.intern
  User pi
```

- Ändern sie den Host Eintrag (erste Zeile) zu `Raspi_Grundlagen`

4. Laden Sie darauf ihre remote-hosts neu (↻)
5. Verbinden Sie sich nun mit ihrem Raspi. Indem Sie ``Connect in new Window`` drücken:

![Remote_Hots](screenshots/ssh_connect_vscode.PNG)

- Falls Sie vorhin **Verbindungsprobleme** hatten, erstellen Sie stattdessen eine **neue Verbindung**, indem Sie ``+`` drücken
- Geben Sie als Ziel die **statische IP**, ihres Raspis ein (siehe Kapitel [Verbindungsprobleme](#verbindungsprobleme))

7. **Erstes Verbinden (für Alle):**
- Es erscheinen nun mehrere **Pop-Ups** oben in der Mitte.
- Wählen Sie ``Linux`` als OS an.
- Geben Sie das Passwort (``raspberry``) ein.
- Die erste verbindung kann 1-2 min dauern, da ein VS Code Server auf dem Raspi installiert werden muss.
- Danach sollten Sie unten links folgende Meldung haben (in grün oder blau):

![great_success_I](screenshots/ssh_connection_3.PNG)

![great_success_II](screenshots/vs_code_great_success.PNG)


9. Gratuliere, Sie haben sich mit dem Raspi erfolgreich verbunden! 
10. Öffnen Sie nun den File Explorer um alle Dateien auf dem Raspi zu sehen:

![open_file_explorer](screenshots/vs_code_open_folder.PNG)

12. Drücken Sie dann ``OPEN FOLDER`` und öffnen Sie den Ordner `home/pi`. Drücken Sie dann auf `OK`:

![open_folder2](screenshots/vs_code_open_folder_2.PNG)

12. Eventuell müssen Sie ihr Passwort (`raspberry`) nochmals eingeben.

13. **Herzlichen Glückwunsch**, Sie haben nun Zugriff auf die Dateien auf dem Raspi

### Extensions installieren

1. Installieren Sie nun die Python Extensions auf ihrem Raspi. Gehen Sie dafür in den **Extension-Tab** von VS Code und suchen Sie nach den aufgelisteten Extensions. Wählen Sie zur Installation immer `install in SSH:`

1. **Python Extension**

![ssh_extensions](screenshots/ssh_extensions.PNG)

2. **Jupyter Extension**

![jupyter_extension](screenshots/ssh_extension_II.PNG)


3. **ipykernel - Jupyter Kernel**
- Aktivieren Sie  den Jupyter Kernel für ihre Jupyter Notebooks indem Sie `sudo apt install python3-ipykernel` in einem **Terminal** eingeben:

![ipykernel_install](screenshots/extensions_terminal.PNG)

### Module / Libraries installieren

Installieren Sie nun die wichtigsten Libraries auf ihrem Raspi. 

1.**Sense-Hat Library:**

- SenseHat sollte bereits installiert sein. Prüfen Sie dies mit: `pip show sense-hat`

![sense-hat-lib](screenshots/sense_hat_library.PNG)

2. **Numpy:**
- Numpy sollte auch bereits installiert sein, prüfen Sie dies mit: `pip show numpy`


![numpy_version](screenshots/numpy_version.PNG)

<!-- 3. **Matplotlib:**
- Installieren Sie nun noch Matplotlib. Sie müssen dafür den folgenden Befehl verwenden: `sudo apt-get install python3-matplotlib`

![sudo_apt_get](screenshots/library_1.PNG) -->

3. Gratuliere, Sie sind fertig mit ihrem Raspi Setup. **Helfen** Sie nun ihren Pult-Nachbarn.


# Verwenden von VS Code mit dem Raspi

Solange Sie unten links die grüne/blaue Meldung sehen (siehe Bild), zeigt dies, dass Sie via ``ssh`` verbunden sind und sich auf dem Raspi.
- Hier können Sie wie gewohnt mit VS Code arbeiten, programmieren, **Files erzeugen** und `.py` Files ausführen.

![great_success](screenshots/vs_code_great_success.PNG)

Speziell ist jedoch, dass sie sich in diesem VS Code Tab **NUR** auf dem Raspi befinden. Heisst dass:
- Alle erstellen Files **NUR** auf dem Raspi sind.
  - Wenn Sie ein File herunterladen wollen, **rechts-klicken** auf das gewünschte File und drücken sie `herunterladen`.
- Die Kommando-Zeile in VS Code nun die Kommando-Zeile vom Raspi ist. Sprich alle Befehle werden **AUF** dem Raspi ausgeführt.
- Wenn Sie ein File von Ihrem Computer hochladen wollen, können Sie dieses einfach per **Drag-n-Drop** in die Ordnerstruktur auf VS Code ziehen und es wird automatisch hochgeladen.
- Sie alle Extensions von VS Code nochmals **AUF** ihrem Raspi installieren müssen (Jupyter und Python Extension).
- Üben wir dies gleich:


1. Pyhton Shell auf dem Raspi:
- Sie können nun die wie normal ein neues Terminal auf dem Raspi öffnen
- Dies erlaubt ihnen mit dem Befehl `python` eine Python **Konsole** auf dem Raspi öffnen
- eben Sie dann das Kommando `print("Hello RasPi")` ein
- Herzlichen Glückwunsch, Sie haben nun ein **Hello World** auf der Python Shell des Raspis ausgeführt!
- Schliessen sie nun die Python Konsole mit `exit()`. (öffnet wieder normale Kommandozeile) 

2. Workspace erstellen: 
- Erstellen Sie nun mit **Rechtsclick / New / New_Folder** einen Ordner mit ihrem Kürzel: Fabian Widmer --> ``FaWi``

![new_folder](screenshots/new_folder.PNG)

- Öffnen Sie dann diesen Folder indem Sie unter ``File/Open Folder`` drücken

![open_folder](screenshots/open_folder.PNG)

- Speichern Sie dann diesen Folder nun als Workspace **lokal auf ihrem PC** ab. Gehen Sie dafür auf ``File/Save Workspace as`` 
- Drücken Sie nun  ``Show Local `` um den Workspace auf ihrem PC (und nicht auf dem Raspi) zu speichern

![show_local](screenshots/show_local.PNG)

3. Python File erstellen
- Erstellen Sie ein File ``hello_world.py`` in ihrem neuen Workspace (inkl. hello-world-code)
- Führen Sie dieses aus. (sowohl via Terminal und Run-Button)

4. File herunterladen
- Drücken Sie **Rechtsclick/Download** bei ihrem ``hello_world.py`` File

5. Python Cells ausfürhen
- Editieren Sie ihr ``hello_world.py`` File und fügen Sie Code zellen hinzu:

![code_cells](screenshots/ssh_code_cell.PNG)

- Führen Sie diese mit <kbd>CTRL</kbd>+<kbd>ENTER</kbd> aus.
- Ein **Pop up** fragt sie nun, ob sie den ipykernel installieren wollen. Drücken Sie ``yes`` und warten Sie bis es fertig installiert hat

![ipykernel](screenshots/ipykernel.PNG)

- Erstellen Sie nun ein ``Jupyter Notebook`` mit Code Zellen und führen diese aus.


# Verwendung des Sens-Hats
Das Sense-Hat wurde ebenfalls schon im vorhinein **montiert und getestet**.
Um dieses selber zu testen, führen Sie das im vorhinein aufs Raspi geladene **Sense-hat Hello-World Skript** aus.
- Dieses ist im Ordner ``pi`` zu finden. (nicht in ihrem Kürzel Ordner)
- Machen Sie dies mit: `python sense_hat_hello_world.py  `
- Falls nun **kein Schriftzug** über ihr LED-Display läuft, könnte das Sense-Hat defekt sein. (Siehe Kapitel [Sense-Hat Fehler](#sensehat-fehler))


1. Sense Hat Skript erstellen
- um das Sense-hat verwenden zu können müssen sie wie im Sense-hat Hello-World Skript die **Library importieren** und ein **sense-Objekt erzeugen**:

```python
from sense_hat import SenseHat
sense = SenseHat()
```

- danach können Sie mit dem ``sense`` Objekt arbeiten (mehr dazu in den Folien)
- führen sie zB. mal den Befehl ``sense.show_message("ihr_satz")`` aus, um einen Satz anzuzeigen.

# Optional: WLAN von zu Hause hinzufügen
Sie können das Raspi so konfigurieren, dass es sich ebenfalls mit ihrem Heim-WLAN / Handy Hotspot verbinden kann. 
Wenn Sie dies einstellen, können Sie auch zu Hause mit dem Raspi arbeiten (nicht nur an der HSLU).

1. Öffnen Sie dafür das Terminal auf dem Raspi (zB via VS Code)
2. Gehen sie den Befehl ``sudo raspi-config`` ein
3. Wählen Sie ``System Options`` an

![raspi_config](screenshots/ssh_raspi_config.PNG)

4. Wählen Sie ``Wireless LAN`` an

![wlan_1](screenshots/ssh_wlan_1.PNG)

5. Geben Sie den **Namen** ihres Hotspots / Heimnetzwerkes ein

![wlan_2](screenshots/ssh_wlan_2.PNG)

6. Geben sie das **Passwort** ihres Hotspots heimnetzwerkes ein.
- Achten Sie auf die Gross- und Kleinschreibung

![wlan_3](screenshots/ssh_wlan_3.PNG)

7. Drücken Sie ``OK``
8. Gehen sie auf ``Finish``

![wlan_4](screenshots/ssh_wlan_4.PNG)


9. Überprüfen Sie ihre Einstellungen mit dem folgenden Befehl: ``sudo cat /etc/wpa_supplicant/wpa_supplicant.conf `` im Terminal eingeben
- Es sollte nun ihr WLAN Eintrag in diesem File aufgelistet sein (zusätzlich zum ``EEE`` Netz)

![wlan_5](screenshots/ssh_wlan_5.PNG)

## Verbinden mit dem Raspi (von zu Hause)
Es gibt nun zwei Möglichkeiten wie sie sich von zu Hause aus mit dem Raspi verbinden können. 

- Via Hotspot / WLAN (Siehe [Kapitel](#via-home-wlan--hotspot))
- Via LAN (Siehe [Kapitel](#alternative-via-lan-kabel-ethernet))

Navigieren Sie zum ensprechenden Kapitel für die Anleitung

### Via Home WLAN / Hotspot
0. **Voraussetzung:** Sie haben das Home WLAN / den Hotspot anhand dem vorherigen Kapitel **auf dem Raspi registriert**.

1. Nachdem Sie das Raspi zu Hause eingeschaltet haben, verbindet es sich automatisch mit ihrem Heim-Netzwerk / Hotspot.

2. Herausfinden der Raspi **IP-Adresse**. Hierzu gibt es verschiedene Möglichkeiten. (Siehe nächste Unterkapitel [via Home-WLAN](#ip-in-home-wlan), [via Hotspot](#ip-via-handy-hotspot), [via LAN](#alternative-via-lan-kabel-ethernet))

3. Erstellen eines neuen SSH - Eintrages in VS Code (Siehe Unterkapitel  [Neue SSH Verbindung konfigurieren](#neue-ssh-verbindung-konfigurieren))

##### IP via Handy Hotspot:
- evtl. zeigt ihr Handy die IP ihres Raspis an (Android). 
- alternativ gibt es Apps, welche die IP von allen Geräten in ihrem Hotspot anzeigen kann, zB das App **Network Analyzer**:
  - Apple Network Analyzer:  [Link Apple](https://apps.apple.com/ch/app/network-analyzer/id562315041)
  - Andriod Network Analyzer: [Link Android](https://play.google.com/store/apps/details?id=net.techet.netanalyzerlite.an&hl=en_US&pli=1)

- Nach dem Installieren müssen in den Tab **LAN** gehen und **Scan** drücken, um alle Geräte, welche mit ihrem Hotspot verbunden sind, anzuzeigen:

  ![Scan_Wifi_devices](screenshots/wifi_scan.png)

##### IP in Home WLAN 
Wenn Sie ihr Raspi so konfiguriert haben, dass es sich mit ihrem Home WLAN verbindet, können Sie auch auf Ihr Router zugreiffen um alle verbundenn Geräte anzuzeigen.

- Evtl. können Sie mit dem Befehl `arp -a` (in Powershell /Terminal) alle Geräte in ihrem Netzwerk anzeigen lassen.

- Falls dies nicht funktioniert können Sie versuchen auf den Router zuzugreifen um alle verbundenen Geräte anzuzeigen. Der Router ist typischerweise under der IP `192.168.0.1` oder  `192.168.1.1` erreichbar. Geben Sie diese Adresse in einem Internet Explorer ein um auf den Router zu zugreiffen.

- Letzlich gibt es noch das Programm **Angry IP Scanner** [Link](https://angryip.org/download/). 

![IP_Scan](screenshots/angry_ip_scan.PNG)

Hiermit ist es möglich alle verbundenen Geräte im Netz anzuzeigen.

### Neue SSH Verbindung konfigurieren
Wenn Sie die Raspi IP wissen, müssen Sie nun nur noch einen neuen SSH Eintrag in VS Code (mit `+` in Remote Tab von VS Code erstellen): 


**SSH Befehl:** (Eingabe bei Pop Up nach drücken von `+`)
- `ssh pi@ihre_raspi_ip` (IP zB: `192.168.10.15`)

**Darauf resultierender Eintrag in ssh-config File:**

```
Host Raspi_at_Home
  HostName ihre_Raspi_IP  
  User pi
```

**Achtung**: ihre_Raspi_IP is die herausgefundene IP (zB 192.168.10.43). Diese kann von Tag zu Tag ändern -> Kontrolle mit IP Scanning Tools.

![new_host](screenshots/ssh_new_host.PNG)

### Alternative via Lan Kabel (Ethernet)
Sie können sich auch via **LAN** mit dem Raspi verbinden (Laptop direkt zu Raspi). Wenn Sie dies machen, benötigen Sie folgenden SSH Eintrag in VS Code (mit `+` in Remote Tab von VS Code erstellen): 

**SSH Befehl:** (Eingabe bei Pop Up nach drücken von `+`)
- `ssh pi@eee-w014-XXX.local`


**Darauf resultierender Eintrag in ssh-config File:**
```
Host Raspi_Local
  HostName eee-w014-XXX.local
  User pi
```

- **Achtung:** XXX ist ihre Raspi Nummer (zB `068`)


# Verbindungsprobleme

## Fehlermeldung 1
Falls Sie sich **nicht verbinden konnten**, kann es sein, dass folgendes Problem aufgetreten ist:
Wenn man sich **zu früh** mit dem Raspi verbindet (mit `ssh`, `ping` oder via **VS Code**), bevor es sich em HSLU Netz angemeldet hat, wird seine `eee-Adresse` im Wifi durch den Aufruf belegt.
Dies führt dazu, dass das Raspi für **30min** nicht mehr via seine `eee-Adresse` (zB. `eee-w014-XXX.simple.eee.intern`) erreichbar ist.


Verbinden Sie sich nun stattdessen mit der eigentlicher `IP` des Raspis
- fragen Sie dazu Ihren Dozenten für den EEE - Eintrag
- BSP IP: `10.180.255.12`  (Einsetzen anstatt `eee-w014-XXX.simple.eee.intern`)

![new_host](screenshots/ssh_new_host.PNG)


## Fehlermeldung 2

Eine typische Fehlermeldung ist die:  `Could not establish connection` Meldung mit dem Log `Ein Prozess hat versucht, zu einer nicht bestehenden Pipeline zu schreiben` (siehe Bild)

![Fehlermeldung_SSH_2](screenshots/ssh_problem_2.PNG)

Lösen Sie dieses Problem wie folgt:
1. Navigieren Sie in Windows zu ihrem **SSH - CONFIG FILE (known_hosts.txt)** dieses sollte unter `C:\Users\"IHR BENUTZERNAME" \.ssh\known_hosts` zu finden sein.

![SSH_Problem_2_1](screenshots/ssh_problem_2_1.PNG)

2. Öffnen Sie das File `config` mit einem Text Editor (zB `Notepad++`)
3. Löschen Sie den Eintrag ihres Raspis darin (zB von `eee-03268.simple.eee.intern`)
- Sie können auch gleich das File löschen
4. Schliessen & Öffnen Sie VS Code
5. Versuchen sie sich erneut zu verbinden 

![reconnect_vs_code](screenshots/vs_code_reconnect.PNG)

6. Bei bestehendem Fehler fragen Sie ihren Dozenten

## Fehlermeldung 3 - DNS Spoofing Attack

Falls Sie folgende Fehlermeldung erhalten, müssen Sie ebenfalls in ihrem `known_hosts.txt` File den **Eintrag von ihrem Raspi löschen löschen**.

![DNS_Spoofing](screenshots/dns_spoofing.PNG)

- Dieses `known_hosts.txt` File finden Sie unter `C:\Users\"IHR BENUTZERNAME" \.ssh\known_hosts`

![known_hosts](screenshots/known_hosts.PNG)

- Versuchen Sie sich dann nochmals zu verbinden
- Falls Sie noch immer Probleme haben, fragen sie ihren Dozenten


## SenseHat Fehler
Das SenseHat kann auf mehrere Arten "gestört" sein.
Bearbeiten Sie dies nachfolgenden Kapitel nacheinander ab, wenn Sie Probleme mit dem Display haben:

### SenseHat Library
1. Überprüfen Sie, ob die sense hat Library installiert ist: ``pip show sense-hat``

![pip_show](screenshots/pip_show.PNG)

2. Wenn diese **nicht installiert ist**, installieren sie diese: ``pip install sense-hat``
3. dann Raspi rebooten & neu verbinden & nochmals testen

### I2C nicht aktiviert
``I2C`` ist nicht aktiviert:
1. Vergewissern Sie sich ob auf ihrem Raspi das `I2C` Interface aktiviert ist (Für Sense Hat).
2. Geben sie dazu den Befehl `sudo raspi-config nonint get_i2c` ein.
- Eine ``1`` bedeutet dabei dass `I2C` **deaktiviert** ist:
3. Falls dies der Fall ist, müssen Sie `I2C` aktivieren.
- Geben Sie dazu `sudo raspi-config nonint do_i2c 0` ein.
4. Dann Raspi rebooten & neu verbinden & nochmals testen

### Raspi forcieren SenseHat zu erkennen (scetchy)
Sie können das Raspi zwingen das SenseHat zu erkennen. Ist etwas heikel.
1. Geben Sie dazu ``sudo sh -c "echo 'dtoverlay=rpi-sense' >> /boot/config.txt" `` ein
2. dann Raspi rebooten & neu verbinden & nochmals testen

## Last-Solution SenseHat wechseln
Letzte Lösung: Neues SenseHat montieren und neu probieren


