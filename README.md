# Erklärung GitLab
Dieses Gitlab-Repository enthält mehrere Anleitungen und Skripte für das Aufsetzen, Verbinden und Verwenden eines Raspis an der Hochschule Luzern Technik & Architektur. Zudem ist die Installation von VS Code dokumentiert.


## Raspi Aufsetzen (für Dozierende)
Das Aufsetzten des RaspberryPis (von Grund auf) ist in der folgenden Anleitung dokumentiert: [Anleitung_Raspi_Aufsetzten](/00_Raspi/Dozenten_Setup/README_Dozent.md). 
> Diese Anleitung ist für Dozierende & Assistierende gedacht, welche ein neues Raspi aufsetzen müssen.


## Raspi verwenden (für Studierende)
Die Verwendung des Raspis ist in dieser Anleitung dokumentiert: [Anleitung_Raspi_Verwenden](/00_Raspi/Student_Setup/README_Students.md).
Sie enthält Beschriebe für:
- Das erste Verbinden
- Verbinden mit VS Code
- Installieren von Extensions
- Installieren von Libraries

## SenseHat
Die Montage des SenseHats ist in dieser Anleitung beschrieben: [Anleitung_SenseHat](/01_SenseHat/ReadMe.md).

## Linux Commands
Eine Sammlung der gängigsten Linux Befehle ist hier zu finden: [Sammlung_Linux_Befehle](/03_Linux/linux_commands.md)

## Digital Engineers Only 
Für die Digital Engineers wurde ebenfalls folgende Zusammenfassungen / Anleitungen geschrieben

### Mini Car
Anleitung für Inbetriebnahme des [Mini Cars](/05_Mini_Car/README.md)

## Kamera
Anleitung zur Inbetriebnahme der [Kamera](/06_Camera/)

### IBM Setup
Hier befindet sich ein [Setup - Skript für IBM](/04_IBM/workshop_setup.py)

