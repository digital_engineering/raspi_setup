[[_TOC_]]

# Raspberry Pi Mini Car Roboter

## Komponenten
![Minicar Boards](img/Carfront.jpeg){width=40%}
![Minicar BoardsII](img/Carback.jpeg){width=40%}

Der Minicar entsteht, wenn du dein Raspberry Pi (orange markiert, auf dem Bild gestackt mit Powerboard) auf das Fahrgestell montierst (z.B. mit doppelseitigem Klebeband). Für die Ansteuerung müssen die entsprechenden Pins von Raspi (GPIO) und Motorentreiber verbunden werden.

### Power board Raspberry Pi
Anzeige Akkuladestand:
![Power Board Ladestand](img/Power_board1.jpg){width=40%}

Das Raspberry Pi wird mit dem Powerboard verschraubt. Es wird von diesem gespiesen und wird so ein mobiles System.

Das Powerboard speist auch die Motoren des Minicars.

Um das Powerboard zu landen, kann der Powerboard USB-C Port auf der Längsseite des Powerboards verwendet werden, und das Raspi Netzteil.

## Bauanleitung
1. Powerboard und Raspi zusammen verschrauben
2. Fahrgestell zusammenbauen
3. Raspi Stack und Fahrgestell z.B. mit doppelseitigem Klebeband verbinden

### Fahrgestell
1. Achtung, Räder wie auf dem Bild herum montieren, sonst kippt der Mini Car nach hinten
2. Motorcontroller und Raspi montieren, z.B. mit Klettverschluss

![Fahrgestell und Motorcontroller](img/Carchassi.jpeg){width=40%}


### Verkabelung und Ansteuerung
Die Verkabelung ist abhängig von der Konfiguration, die in der Software erstellt wird.
Über die H-Brücke werden Richtung und Geschwindigkeit (mit PWM) (bzw. nur Richtung, vorwärts oder rückwärts, bei digitalem Signal ein/aus) gesteuert.

#### Python Library
Für GPIO Ansteuerung kann die Library  [RPi.GPIO](https://pypi.org/project/RPi.GPIO/) verwendet werden.

Sie wird installiert mit
```
sudo apt-get -y install python3-rpi.gpio
```

#### Raspi -> H-Brücke
Das Raspi steuert die H-Brücke mit 4 GPIO Ausgängen. Ground wird auch verbunden, die Speisung der Motoren erfolgt über einen "Power Pin" des Raspis.

Beispiel Code, der die Motoren digital und einen Motor mit PWM (Pulsweitenmoduliert, für unterschiedliche Geschwindigkeiten) ansteuert [hier](https://gitlab.switch.ch/digital_engineering/raspi_setup/-/blob/refactor23/06_Mini_Car/minicar_testing.py) in diesem Repo.

Für den Beispielcode wird der Motor Controller so mit dem Raspberry Pi verbunden:
- Ground: Pin 9 (graues Kabel auf dem Foto)
- GPIO: 19 (Pin 35), 16 (Pin 36), 26 (Pin 37), 20 (Pin 38)
- Spannung: Raspi Power Pin 2 (5V, rotes Kabel auf dem Foto). Eine rote LED zeigt an, dass der Motorcontroller gestartet ist.

Pinout Raspberry Pi GPIO Steckerleiste (inkl. Power (rot) und Ground (grau)):

![Pinout Raspberry Pi 4](img/Raspi4_pins_doc.jpg){width=60%}
![GPIO Verbindungen](img/Schema.jpg){width=60%}

Anschluss Motorcontroller: Aussen sind je zwei Pins für die Motorensteuerung, innen sind Ground (GND) und Speisung (VCC).
![Controller Anschluss](img/car5_controller_2_raspi.png){width=40%}

#### H-Brücke -> Motoren
Zwei Kabel werden von der H-Brücke zu jedem Motor geführt (auf den Bildern ein weisses und ein graues Kabel zum Motor 1):
![H-Brücke Ausgang zu Motor 1](img/car4_controller.png){width=40%}
![Motor Anschluss](img/car3_motoren.png){width=40%}

Welcher Pin zu welchem Motoranschluss führt bestimmt die Drehrichtung der Motoren. Wenn die Motoren anders herum drehen als erwartet, können die Pins einfach getauscht werden.

Analog wird Motor2 angeschlossen. 

#### Ultraschallsensor
Der Ultraschallsensor wird so an Speisung und Signale angeschlossen:
![GPIO -> Ultraschall](img/gpio_us.png){width=50%}

Für Trigger und Echo können auch andere GPIO Pins verwendet werden. Speisung: 5V.

Bespielcode für den Ultraschallsensor [hier](https://gitlab.switch.ch/digital_engineering/raspi_setup/-/blob/refactor23/06_Mini_Car/us.py) in diesem Repo.

## Inbetriebnahme
1. Test Files auf Raspberry Pi kopieren (z.B. mit scp (secure copy Befehl)
2. **Minicar vom Tisch nehmen und auf den Boden stellen**
3. Test files ausführen:
```
python3 minicar_testing.py
```
Bzw. 
```
python3 us.py
```

### Ein- und ausschalten
Wenn das Raspi Power Board geladen ist, kann das Raspi über den Taster auf dem Power Board gestartet werden.

Ausschalten mit
```
sudo shutdown now
```
und Passwort.

Anschliessend noch einmal Taster drücken, um das Power Board auszuschalten. (Die LED Anzeige erlischt nach ein paar Sekunden.)

# Mögliche Probleme
## Python Modul nicht installiert, nachinstallieren
Dafür muss das Raspberry Pi eine Verbindung zum Internet haben. Ob das der Fall ist, kann mit einem ping an google getestet werden:
```
ping 8.8.8.8
```
Wenn der google server nicht erreichbar ist, das Raspi (am einfachsten über Raspi Desktop Netzwerk Config) über WLAN (z.B. Handy Hotspot, EEE oder Public) mit dem Internet verbinden.

## Raspi Power Board Akkus leer?
Der Akkuladestand von Raspi Power Board kann am Powerboard abgelesen werden.
Wenn die Akkus leer sind mit USB-Micro Kabel (Buchse (5) Batterieseite) oder USB-C Kabel (Buchse (9) Rückseite) an eine Spannungsversorgung anschliessen und laden.

Zum (schnell) Laden muss das Akku Board vom Raspi getrennt werden.

## Keine Verbindung zum EEE-Netz?
Das Raspi-Setup Skript funktioniert für Ubuntu nicht, deshalb müssen die Credentials für das EEE-Netz mittels dem eingebauten NetworkManger eingetragen werden:
1. Wechsle in das Verzeichnis `/etc/NetworkManager/system-connections`
2. Editiere darin die Verbindung `EEE.nmconnection` und trage dort die Credentials vom EEE-Portal entsprechend ein.

Oder über Desktop/GUI:
![Einstellungen EEE](img/eee_settings_network_manager_gui.png)

### Troubleshooting 
**EEE Credentials**: Überprüfe, dass keine unsichtbaren Zeichen kopiert wurden!

## GPIO Zugriffsfehler
Für Ubuntu 20.04 müssen je nach verwendetem Image weitere Schritte ausgeführt werden:
```
pip install RPi.GPIO
sudo apt install rpi.gpio-common
sudo adduser air dialout
sudo reboot
```

[Link zum Forum/Problem](https://askubuntu.com/questions/1230947/gpio-for-raspberry-pi-gpio-group) 

## Sensehat IMU in Ubuntu verwendeten

Standardprogramme für das Sensehat kann man über folgende Website erhalten:
https://projects.raspberrypi.org/en/projects/getting-started-with-the-sense-hat/8

Es kann auftreten, dass die Rückgabewerte der Sensoren, beispielsweise des Accelometers, immer 0 betragen. In diesem Fall ist folgende Anleitung zu verfolgen:
Der Teil "Raspberry Pi Setup" und der Teil "Sense HAT Setup"

Für das Raspberry Pi Setup werden folgende Schritte durchgeführt:

öffne das usercfg.txt File
```
cd /boot/firmware
sudo nano usercfg.txt
```

Füge folgende Zeilen am Ende des usercfg.txt Files ein:
```
hdmi_force_hotplug=1 # Allows RPi to boot in headless mode with Sensor HAT installed.
dtparam=i2c_arm=on   # Enables auto loading of i2c module.
```
Starte das Raspberry Pi neu
```
sudo reboot 
```

installiere falls nötig folgende Bibliotheken:
```
sudo apt install python3 python3-dev python3-pip python3-venv  \
                i2c-tools openssl
```
öffne das modules File und füge die folgende Zeile am Ende ein:
```
sudo nano /etc/modules

i2c-dev
```

Erstelle eine neue udev Regel und füge untenstehenden Text ein:
```
cd /etc/udev/rules.d/
sudo touch 99-i2c.rules
sudo nano 99-i2c.rules

KERNEL=="i2c-[0-7]",MODE="0666"
```

Erstelle ein neues Blacklist File und füge untenstehende Zeilen ein:

```
cd /etc/modprobe.d/
sudo touch blacklist-industialio.conf
sudo nano blacklist-industialio.conf

blacklist st_magn_spi
blacklist st_pressure_spi
blacklist st_sensors_spi
blacklist st_pressure_i2c
blacklist st_magn_i2c
blacklist st_pressure
blacklist st_magn
blacklist st_sensors_i2c
blacklist st_sensors
blacklist industrialio_triggered_buffer
blacklist industrialio
```

Starte das Raspberry Pi neu und schliesse die Sense HAT Einstellungen ab. 




## Popup "Authentication required to create Managed color device"

Falls immer das "nervige" Popup auftaucht und nach den Anmeldedaten verlangt für die Authentication für das kreieren eines gemanagten Devices kann die anleitung im untenstehenden Link befolgt werden. Es muss dafür ein File erstellt werden in einem spezifischen Ordner. 

`sudo nano /etc/polkit-1/localauthority/50-local.d/45-allow-colord.pkla`

Dann folgenden Code in das File kopieren:

```
[Allow Colord all Users]
Identity=unix-user:*
Action=org.freedesktop.color-manager.create-device;org.freedesktop.color-manager.create-profile;org.freedesktop.color-manager.delete-device;org.freedesktop.color-manager.delete-profile;org.freedesktop.color-manager.modify-device;org.freedesktop.color-manager.modify-profile
ResultAny=no
ResultInactive=no
ResultActive=yes

```

File verlassen und speichern. Raspberry Pi neustarten und das Problem sollte verschwunden sein.

[Link zum Authentication Fehler](https://c-nergy.be/blog/?p=14051)

# Weiterführende Links
- [H-Brücke / Motorsteuerung](https://de.wikipedia.org/wiki/Vierquadrantensteller)
- [Video über den verwendeten Motorcontroller, mit Arduino angesteuert](https://www.youtube.com/watch?v=4opVl-aT28U)
- [Raspi Doku](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html)
- Raspi Doku: [Terminal Ausgabe des Pinout](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#gpio-pinout)
- Raspi Doku: [weitere GPIO Library](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#gpio-in-python)
