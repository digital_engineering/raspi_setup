### Meist gebrauchte Bash-Kommandos:

|Kommando &nbsp; &nbsp; | Befehl                                                                                            | Beschreibung                                              |
|---|---------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
|pwd | print working directory                                                                           | Aktuelles Verzeichnis anzeigen                            |
|cd [Pfad] | change directory                                                                                  | Verzeichnis wechseln                                      |
|Bsp: cd /home/pi | absoluter Pfad                                                                                    |
|Bsp: cd meinOrdner | relativer Pfad                                                                                    |
|Bsp: cd .. | Eine Hierarchie im Verzeichnis hoch                                                               |
|ls | list                                                                                              | Inhalt anzeigen von aktuellen Verzeichnis                 |
|ll | list long                                                                                         | Inhalt anzeigen mit zusätzlichen Informationen            |
|lastlog| Überprüfen, wann sich ein Benutzer zuletzt eingeloggt hat                                         ||
|mkdir [Name] | make directory                                                                                    | Ein Verzeichnis erstellen                                 |
|rmdir [Name]  | remove directory                                                                                  | Ein Verzeichnis löschen                                   |
|rm [Name] | remove                                                                                            | Ein File löschen                                          |
|rm -r | remove recursive                                                                                  | Rekursives Löschen                                        |
|rmdir  | remove directory                                                                                  | Löschen leeres Verzeichnis                                |
|mv [Name] [Verzeichnis]| move                                                                                              | Ein File verschieben                                      |
|ifconfig | Interface configuration                                                                           | Anzeigen der IP Adressen                                  |
|iwconfig| Informationen zum WLAN anzeigen                                                                   |
|top  | Taskmanager                                                                                       |
|sudo [Name] | run as super user                                                                                 | Als Administrator ausführen                               |
|uname -a | Kernel Version anzeigen                                                                           |
|touch [Name] | leere Datei erstellen oder Datum aktualisieren                                                    |
|ping www.google.com | Internetverbindung prüfen                                                                         |
|sudo reboot | Systemneustart                                                                                    |
|Ctrl+C | Einen laufenden Prozess beenden                                                                   |
|history | Kommando History anzeigen                                                                         |
|!<nr> | !17  Kommando in History ausführen                                                                |
|!! | Letztes Kommando ausführen, oder auch !-1                                                         |
|CTRL+R | reverse-i-search                                                                                  | Suchen in History                                         |
|clear | Konsole löschen                                                                                   | (eigentlich Scrollen) oder CTRL+L                         |
|grep <pattern>| history / grep ls                                                                                 | Suchen nach Zeichen                                       |
|whoami | Wer bin ich                                                                                       | Aktueller Benutzernamen                                   |
|cp [Name] | copy                                                                                              | Kopieren von Dateien und Verzeichnissen                   |
|cat | concatenate                                                                                       | Zusammenfügen von Dateien und auf Konsole  ausgeben       |
|man | manual                                                                                            | Hilfe zu Befehlen                                         |
|more [Name] | more file.txt                                                                                     | Seitenweise Ausgabe von Text und Dateien                  |
|less | ls -al / less                                                                                     | Ähnlich wie ’more’, aber man kann vor- und  zuruckblättern |
|tail | Ausgabe Dateiende                                                                                 |
|which | Wo befindet sich ein Programm                                                                     |
|kill | Prozess terminieren                                                                               |
|ps | ps -aux                                                                                           | Prozesse auflisten                                        |
|df | disk free                                                                                         | Belegung Speicherplatz                                    |
|free | Belegung Memory                                                                                   |
|top | Taskliste                                                                                         | htop  ’graphische’ Taskliste                              |
|sudo iwlist wlan0 scan | Offene Netzwerke anzeigen                                                                         |
|sudo iwlist wlan0 scan | grep SSID Alle ESSID listen                                                                       |
|ip link show wlan0 &#124; grep ether | MAC-Adresse vom WLAN anzeigen                                                                     ||
|sudo apt-get update| Paketinformationen aus allen konfigurierten Quellen herunterladen                                 |
|sudo apt-get upgrade| verfügbare Upgrades aller derzeit auf dem System installierten Pakete installieren                |
|sudo apt-get autoremove | entfernt Pakete, die automatisch installiert wurden, weil sie von anderen Paketen benötigt wurden |
|sudo apt-get autoclean| löscht das lokale Repository von abgerufenen Paketdateien                                         |
|upower --dump| Zeigt alle Stromversorgungsgeräte und deren Statistik|                                                           |
|xkill|Tasks oder Prozesse killen|
|||


### Linux Dateisystem
![Linux_Dateisystem](04_Linux/screenshots/linux_dateisystem.png)
|Verzeichnis|Bedeutung|
|---|---|
|bin| Programme die während dem Systemstart verfügbar sind (user, root)|
|boot| beinhaltet Dateien (Kernel) zum Starten des Systems|
|dev|Gerätedateien|
|etc| Konfigurationsdateien|
|home| Benutzerverzeichnisse werden unterhalb von /home abgelegt|
|lib| Bibliotheken|
|lost+found|Teilweise wiederhergestellte Dateien|
|media| Verzeichnis zum Einbinden von CD-ROMs, USB-Sticks etc.|
|mnt| Weiteres Verzeichnis zum Einbinden von Laufwerken (für Admin)|
|opt|Für Softwarepakete die nicht von der Distribution stammen|
|proc| Virtuelles Verzeichnis mit Infos vom Kernel|
|root| Benutzerverzeichnis vom Benutzer root|
|run| Beinhaltet Laufzeitdaten von Programmen & Diensten|
|sbin|Programme die während dem Systemstart verfügbar sind (root)|
|srv| Selten gebraucht für Daten von Webserver, FTP-Server etc.|
|sys| Virtuelles Verzeichnis mit Infos zur Hardware|
|tmp| Temporäres Verzeichnis, wird beim Systemstart gelöscht|
|usr| Beinhaltet installierte Software|
|var| Verzeichnis für Dateien die gelesen und geschrieben werden|

### Linux Berechtigungssystem

In Linux gibt es unterschiedliche Berechtigungen:
* Lesen (read), Abkürzung ’r’
* Schreiben (write), Abkürzung ’w’
* Ausführen (execute), Abkürzung ’x’

Diese drei Berechtigungen gibt es für jede Datei drei Mal:
* User: Berechtigungen für den Besitzer der Datei
* Group: Berechtigungen für eine ausgewählte Benutzergruppe
* Other: Berechtigungen für alle anderen

![Linux_Berechtigungssystem](/03_Linux/screenshots/linux_berechtigungssystem.png)

Die Berechtigung kann mittels `chmod` geändert werden.
Der erste Parameter definiert die zu ändernden Rechte:
1. u(user), g(group), o(other) einzeln, eine Kombination oder a(all) für alle.
2. ’+’ um Rechte hinzuzufügen, ’-’ um Rechte zu entfernen, oder ’=’ um die
Berechtigung zu  überschreiben.
3. ’r’, ’w’, ’x’ einzeln oder eine Kombination davon

Beispiel: `chmod ug+rw myfile.txt` \
Alternativ kann auch direkt ein binär codierter Wert gesetzt werden: \
Beispiel: `chmod 753 myfile.txt`
